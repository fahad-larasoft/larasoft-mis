<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>Larasoft</title>

    <style type="text/css">
        #outlook a{
            padding:0;
        }
        body{
            -webkit-text-size-adjust:none;
            margin: 0px;
            padding: 30px;

        }

        body p a {
            font-weight: bold;
            color: black;
            text-decoration: underline;
        }


        body p span {
            font-weight: bold;
            color: black;
        }

        img{
            border:0;
            height:auto;
            line-height:100%;
            outline:none;
            text-decoration:none;
        }
        table td{
            border-collapse:collapse;
        }


        @media only screen and (max-width: 480px){
            table.container{
                width:100% !important;
            }

        }




    </style>
</head>


<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" bgcolor="#dfdfdf">
<table cellpadding="0" cellspacing="0" border="0" width="600" bgcolor="white" style="box-shadow: 0px 0px 30px #d0d0d0; margin: 0 auto;" class="container">
    <tr>
        <td width="50"/>
        <td height="50"/>
        <td width="50"/>
    </tr>

    <tr>
        <td width="50"/>
        <td>
            <table cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td><a href="{{ route('register') }}?email={{ $invitation->email }}&code={{ $invitation->code }}"><img src="https://i2.wp.com/www.sweetrepeats.org/registernow.gif?zoom=2" alt="Larasoft" /></a></td>
                </tr>
                <tr>
                    <td height="50"/>
                </tr>
                <tr>
                    <td>
                        <p style="font-family: Arial, sans-serif; font-size: 16px; color: black; line-height: 21px;">
                            Dear,
                            <br /><br />
                            We're a UK based software development company that specialises in Laravel and iOS apps.

                            We manage cloud software projects from concept to execution, using our deep and broad experience.

                            Don't worry, we don't cost the earth, in fact, we offer great rates to suit any budget.
                            <br /><br />
                            Credentials
                            <br/>
                            Email : {{ $invitation->email }}
                            <br/>
                            <br /><br /><br />
                            Sincerely
                            <br /><br />
                            Larasoft Admin<br />
                            Larasoft
                        </p>
                    </td>
                </tr>
            </table>
        </td>
        <td width="50"/>
    </tr>

    <tr>
        <td width="50"/>
        <td height="50"/>
        <td width="50"/>
    </tr>


</table>
</body>
</html>