@extends('adminlte::master')

@section('adminlte_css')
    <link rel="stylesheet"
          href="{{ asset('vendor/adminlte/dist/css/skins/skin-' . config('adminlte.skin', 'blue') . '.min.css')}} ">
    @stack('css')
    @yield('css')
@stop

@section('body_class', 'skin-' . config('adminlte.skin', 'blue') . ' sidebar-mini ' . (config('adminlte.layout') ? [
    'boxed' => 'layout-boxed',
    'fixed' => 'fixed',
    'top-nav' => 'layout-top-nav'
][config('adminlte.layout')] : '') . (config('adminlte.collapse_sidebar') ? ' sidebar-collapse ' : ''))

@section('body')
    <div class="wrapper">

        <!-- Main Header -->
        <header id="main-header" class="main-header">
            <!-- Logo -->
            <a href="/" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini">{!! config('adminlte.logo_mini', '<b>L</b>S') !!}</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg">{!! config('adminlte.logo', '<b>Larasoft</b>MIS') !!}</span>
            </a>

            <!-- Header Navbar -->
            {{--<nav id="header" class="navbar navbar-static-top" role="navigation">--}}
                {{--<!-- Sidebar toggle button-->--}}
                {{--<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">--}}
                    {{--<span class="sr-only">{{ trans('adminlte::adminlte.toggle_navigation') }}</span>--}}
                {{--</a>--}}

                {{--<!-- Navbar Right Menu -->--}}
                {{--<div class="navbar-custom-menu">--}}

                    {{--<ul class="nav navbar-nav">--}}
                        {{--<li><a>{{auth()->user()->name}}</a></li>--}}
                        {{--<li>--}}
                            {{--@if(config('adminlte.logout_method') == 'GET' || !config('adminlte.logout_method') && version_compare(\Illuminate\Foundation\Application::VERSION, '5.3.0', '<'))--}}
                                {{--<a href="{{ url(config('adminlte.logout_url', 'auth/logout')) }}">--}}
                                    {{--<i class="fa fa-fw fa-power-off"/> {{ trans('adminlte::adminlte.log_out') }}--}}
                                {{--</a>--}}
                            {{--@else--}}
                               {{----}}
                            {{--@endif--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</nav>--}}



            <nav id="header" class="navbar navbar-static-top">
                {{--<div class="navbar-custom-menu">--}}
                    {{--<ul class="nav navbar-nav">--}}

                        {{--<!-- User Account: style can be found in dropdown.less -->--}}
                        {{--<li class="dropdown user user-menu">--}}
                            {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown">--}}
                                {{--<img src="{{ auth()->user()->image_url }}" class="user-image" alt="User Image">--}}
                                {{--<span class="hidden-xs">{{ auth()->user()->name }}</span>--}}
                            {{--</a>--}}
                            {{--<ul class="dropdown-menu">--}}
                                {{--<!-- User image -->--}}
                                {{--<li class="user-header">--}}
                                    {{--<img src="{{ auth()->user()->image_url }}" class="img-circle" alt="User Image">--}}

                                    {{--<p>--}}
                                        {{--{{ auth()->user()->name }}--}}
                                        {{--<small>Member since {{ auth()->user()->created_at }}</small>--}}
                                    {{--</p>--}}
                                {{--</li>--}}
                                {{--<!-- Menu Footer-->--}}
                                {{--<li class="user-footer">--}}
                                    {{--<div class="pull-left">--}}
                                        {{--<a href="#" class="btn btn-default btn-flat">Profile</a>--}}
                                    {{--</div>--}}
                                    {{--<div class="pull-right">--}}
                                        {{--<form action="/logout" method="post">--}}
                                            {{--<button type="submit" class="btn btn-default btn-flat">Sign out</button>--}}
                                        {{--</form>--}}
                                    {{--</div>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            </nav>
        </header>

        @if(config('adminlte.layout') != 'top-nav')
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">

            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">

                <!-- Sidebar Menu -->
                <ul class="sidebar-menu" data-widget="tree">
                    @each('adminlte::partials.menu-item', $adminlte->menu(), 'item')
                </ul>
                <!-- /.sidebar-menu -->
            </section>
            <!-- /.sidebar -->
        </aside>
        @endif

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @if(config('adminlte.layout') == 'top-nav')
            <div class="container">
            @endif

            <!-- Content Header (Page header) -->
            <section class="content-header">
                @yield('content_header')
            </section>

            <!-- Main content -->
            <section class="content">

                @yield('page-content')

            </section>
            <!-- /.content -->
            @if(config('adminlte.layout') == 'top-nav')
            </div>
            <!-- /.container -->
            @endif
        </div>
        <!-- /.content-wrapper -->

    </div>
    <!-- ./wrapper -->
@stop

@section('adminlte_js')
    <script src="/js/app.js"></script>
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script src="/js/plugins/loadingoverlay.min.js" type="text/javascript"></script>
    <script>
        axios.interceptors.request.use(function (config) {
            $.LoadingOverlay("show");
            return config;
        }, function (error) {
            $.LoadingOverlay("hide");
            return Promise.reject(error);
        });
        axios.interceptors.response.use(function (response) {
            $.LoadingOverlay("hide");
            return response;
        }, function (error) {
            $.LoadingOverlay("hide");
            return Promise.reject(error);
        });
    </script>
    @stack('js')
    @yield('js')
@stop
