<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title_prefix', config('adminlte.title_prefix', ''))
@yield('title', config('adminlte.title', 'Larasoft'))
@yield('title_postfix', config('adminlte.title_postfix', ''))</title>
    <script>
        window.App = {!!   json_encode([
            'auth_user' => auth()->user(),
            'is_authenticated' => auth()->check(),
            'is_admin' => auth()->check() ? \App\Utils\AuthWrapper::isAdmin(): '',
            'is_staff' => auth()->check() ? \App\Utils\AuthWrapper::isStaff(): '',
            'admin' => auth()->check() ? \App\Utils\AuthWrapper::admin(): '',
            'google_cal_api_key' => config('setting.google_calendar_api_key'),
        ]); !!};
    </script>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/Ionicons/css/ionicons.min.css') }}">

    @if(config('adminlte.plugins.select2'))
        <!-- Select2 -->
        <link rel="stylesheet" href="{{ asset('vendor/adminlte/cdn/css/select2.css') }}">
    @endif

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/AdminLTE.min.css') }}">

    @if(config('adminlte.plugins.datatables'))
        <!-- DataTables -->
        <link rel="stylesheet" href="/vendor/adminlte/cdn/css/jquery.dataTables.min.css">
    @endif

    @yield('adminlte_css')

    <!--[if lt IE 9]>
    <script src="/vendor/adminlte/cdn/js/html5shiv.min.js"></script>
    <script src="/vendor/adminlte/cdn/js/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="/vendor/adminlte/css/fonts/fonts.css">
        <style>
            .modal-open .modal {
                display: block;
            }
        </style>
</head>
<body class="hold-transition @yield('body_class')">

@yield('body')
<script src="{{ asset('vendor/adminlte/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('vendor/adminlte/vendor/jquery/dist/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('vendor/adminlte/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
{{--<script src="/js/app.js"></script>--}}



@if(config('adminlte.plugins.select2'))
    <!-- Select2 -->
    <script src="/vendor/adminlte/cdn/js/select2.min.js"></script>
@endif

@if(config('adminlte.plugins.datatables'))
    <!-- DataTables -->
    <script src="/vendor/adminlte/cdn/js/jquery.dataTables.min.js"></script>
@endif

@if(config('adminlte.plugins.chartjs'))
    <!-- ChartJS -->
    <script src="/vendor/adminlte/cdn/js/Chart.bundle.min.js"></script>
@endif

@yield('adminlte_js')

</body>
</html>
