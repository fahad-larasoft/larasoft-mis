@if (Session::has('success'))

        <div class="alert alert-success" role="alert" id="alert">
            <i class="fa fa-check" style="margin-right: 10px;"/>{{ Session::get('success') }}
        </div>


@endif
@if (count($errors) > 0)
<div class="row">
        <div class="alert alert-danger" role="alert" id="alert-error">
            <strong>Errors:</strong>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
</div>
@endif
@section('script')
    <script src="{{ asset('js/post/post.js') }}"></script>
@endsection