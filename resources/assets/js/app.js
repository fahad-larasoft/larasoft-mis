
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

require('./bootstrap');
import React from 'react';
import { render } from 'react-dom';
import { HashRouter as Router, Route } from 'react-router-dom';
import { HashRouter  } from 'react-router-dom';

/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import SideBar from './components/Sidebar';
import Header from './components/Header';

render((
    <HashRouter>
          <div>
              <Route component={SideBar} />
          </div>
    </HashRouter>
), document.getElementById('sidebar'));

render((
    <HashRouter>
        <div>
            <Route component={Header} />
        </div>
    </HashRouter>
), document.getElementById('header'));


import Dashboard from './components/dashboard';

import Users from './components/users/index';
import CreateUser from './components/users/create';
import UpdateUser from './components/users/edit';
import Profile from './components/users/Profile';
import AdminSalaryIndex from './components/admin/salaries/Index'
import UserSalaryIndex from './components/salaries/Index'
import ProjectIndex from './components/projects/Index'
import AppraisalsIndex from './components/appraisals/Index'
import RatingTypeIndex from './components/rating-types/Index'
import AttendenceIndex from './components/attendance/Index'

import ChangePassword from './components/change-password';


render((
    <HashRouter>
        <div>
            <Route exact path="/" component={Dashboard} />

            <Route exact path="/salaries" component={ AdminSalaryIndex } />
            <Route exact path="/projects" component={ ProjectIndex } />
            <Route exact path="/appraisals" component={ AppraisalsIndex } />
            <Route exact path="/rating-types" component={ RatingTypeIndex } />
            <Route exact path="/attendances" component={ AttendenceIndex } />


            <Route exact path="/users" component={Users} />
            <Route path="/users/create" component={CreateUser} />
            <Route path="/users/:id/profile" component={Profile} />
            <Route path="/users/edit/:id" component={UpdateUser} />

            <Route exact path="/user/:id/salaries" component={UserSalaryIndex}/>

            <Route path="/change-password" component={ChangePassword} />
        </div>
    </HashRouter>
), document.getElementById('lms'));
