import React, {Component} from 'react';
import AppraisalRow from './partials/AppraisalRow';
import axios from 'axios'

class Index extends Component {
    constructor(props) {
        super(props);

        let is_admin = false;
        let user_staff = window.App.is_staff ? window.App.auth_user.staff.id : this.props.user.id;

        this.state = {
            staff_id: is_admin ? this.props.match.params.id : user_staff,
            appraisals: [],
            loading: true,
            messages: [],
            is_admin: window.App.is_admin,
            staff: {},
            modalIsOpen: false,
        };

        this.handleAppraisalUpdate = this.handleAppraisalUpdate.bind(this);
        this.handleMessageUpdate = this.handleMessageUpdate.bind(this);
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    openModal() {
        this.setState({
            modalIsOpen: true
        })
    }

    closeModal() {
        this.setState({
            modalIsOpen: false
        })
    }

    componentDidMount() {
        let react = this;
        let url = `/admin/appraisals`;
        let params = {
            id: react.props.user.id
        };

        axios.get(url, {
            params: params
        })
            .then(response => {
                this.setState({
                    appraisals: response.data.data,
                });
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    handleAppraisalUpdate(salary, statusUpdate) {
        let appraisals = this.state.appraisals;
        if (!statusUpdate.delete) {
            for (let i = 0; i < appraisals.length; i++) {
                if (appraisals[i].id === salary.id) {
                    if (i > -1) {
                        appraisals.splice(i, 1);
                    }
                }
            }
            appraisals.push(salary);
        }

        this.setState({
            appraisals: appraisals
        })
    }

    handleMessageUpdate(message) {
        let react = this;
        let messages = react.state.messages;
        messages.push(message);
        react.setState({
            messages: messages
        });

        setTimeout(function () {
            react.setState({messages: []});
        }, 10000);
    }

    render() {
        let react = this;
        return (
            <div className="tab-pane" id="appraisals">

                <ul className="timeline timeline-inverse">
                    { react.state.appraisals.map(function (appraisal, index) {
                        return (
                           <AppraisalRow key={index} appraisal={appraisal}/>
                        )
                    }) }
                </ul>
            </div>
            
            // <div className="tab-pane" id="appraisals">
            //             <div className="box-header">Appraisals
            //                 <div>
            //                     {react.state.is_admin ?
            //                         <button onClick={react.openModal} className="btn btn-default pull-right"
            //                                 style={createButton}><i className="fa fa-plus"/>&nbsp;
            //                             Create</button> : ''
            //                     }
            //
            //                     <Modal
            //                         show={react.state.modalIsOpen}
            //                         onHide={react.closeModal}
            //                         aria-labelledby="ModalHeader"
            //                     >
            //                         <Modal.Header>
            //                             <Modal.Title id='ModalHeader'>Create Appraisal</Modal.Title>
            //                         </Modal.Header>
            //                         <CreateModal
            //                             openModal={react.openModal}
            //                             closeModal={react.closeModal}
            //                             handleUpdate={react.handleAppraisalUpdate}
            //                             staff_id={react.state.staff_id}
            //                             staff={react.state.staff}
            //                             handleMessagesUpdate={react.handleMessageUpdate}
            //                         />
            //                     </Modal>
            //                 </div>
            //             </div>
            //
            //
            //             <table className="table table-striped">
            //                 <thead>
            //                 <tr>
            //                     <th>Rating type</th>
            //                     <th>Rating Points</th>
            //                     <th>User Comment</th>
            //                     <th>Admin Comment</th>
            //                     <th width="200px">Actions</th>
            //                 </tr>
            //                 </thead>
            //                 <tbody>
            //                 {react.tabRow()}
            //                 </tbody>
            //             </table>
            //         </div>

        );
    }
}

export default Index

