import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import Modal from 'react-bootstrap-modal';

import axios from 'axios';
import PayModal from "./PayModal";
import EditModal from "./EditModal";

class AppraisalRow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modelIsOpen: false
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    openModal() {
        this.setState({
            modalIsOpen: true
        })
    }

    closeModal() {
        this.setState({
            modalIsOpen: false
        })
    }

    handleTotal(){
        let salary = this.props.obj;
        return parseInt(salary.basic_salary || 0) + parseInt(salary.bonus_amount || 0) + parseInt(salary.overtime_amount || 0) - parseInt(salary.deduction_amount || 0);
    }

    handleSubmit(event) {
        event.preventDefault();
        const {history} = this.props;

        let uri = '/users/' + this.props.obj.id;
        axios.delete(uri);
        history.push('/users');
    }

    render() {
        let appraisal = this.props.appraisal;
        return (
            <li key={appraisal.id}>
                <i className="fa fa-star bg-blue"/>

                <div className="timeline-item">
                    <span className="time">{appraisal.points} <i className="fa fa-star"/></span>

                    <h3 className="timeline-header"><a href="#" target="_blank">{ appraisal.rating_type.name }</a></h3>

                    <div className="timeline-body">
                        <b>Admin</b>&nbsp; {appraisal.admin_comment}
                    </div>
                    <div className="timeline-footer">
                        <div className="timeline-body">
                            <b>User</b>&nbsp; {appraisal.user_comment}
                        </div>
                    </div>
                </div>
            </li>

            // <tr>
            //     <td>
            //         {this.props.obj.rating_type.name}
            //     </td>
            //     <td>
            //         {this.props.obj.points}
            //     </td>
            //     <td>
            //         { this.props.obj.user_comment }
            //     </td>
            //     <td>
            //         { this.props.obj.admin_comment }
            //     </td>
            //     <td>
            //         <div>
            //             {window.App.is_admin ?
            //                 <button type="button" className="btn btn-info" onClick={this.openModal}> Edit
            //                 </button> : ''
            //             }
            //             &nbsp;&nbsp;
            //             <Modal
            //                 show={this.state.modalIsOpen}
            //                 onHide={this.closeModal}
            //                 aria-labelledby="ModalHeader"
            //             >
            //                 <Modal.Header>
            //                     <Modal.Title id='ModalHeader'>Pay Salary {this.props.obj.name}</Modal.Title>
            //                 </Modal.Header>
            //                 <PayModal openModal={this.openModal} closeModal={this.closeModal}
            //                           handleUpdate={this.props.handleUpdate}
            //                           salary={this.props.obj}
            //                           handleMessagesUpdate={react.props.handleMessagesUpdate}
            //                 />
            //             </Modal>
            //         </div>
            //
            //     </td>
            // </tr>
        );
    }
}


export default AppraisalRow;