import React, {Component} from 'react';
import Modal from 'react-bootstrap-modal';
import StarRating from 'react-star-rating-component';

import 'react-datepicker/dist/react-datepicker.css';
import Error from "../../common/Error";

class CreateModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            salary: {
                status: 'pending',
            },
            rating: 1,
            errors: [],
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleBasicSalary = this.handleBasicSalary.bind(this);
    }

    handleBasicSalary(e) {
        let salary = this.state.salary;
        salary.basic_salary = e.target.value;
        this.setState({
            salary: salary
        })
    }

    handleSubmit(e) {
        let react = this;
        e.preventDefault();

        let statusUpdate = {
            create: false,
            update: true,
            delete: false,
        };

        let salary = react.state.salary;
        salary.staff_id = react.props.staff_id;

        axios.post('admin/salaries', salary)
            .then(response => {
                react.props.handleUpdate(response.data.data, statusUpdate);
                react.props.handleMessagesUpdate(response.data.message);
                this.props.closeModal();
            })
            .catch(function (error) {
                console.log(error.response.data.errors);
                if (error.response) {
                    react.setState({errors: error.response.data.errors});
                    setTimeout(function () {
                        react.setState({errors: []});
                    }, 4000);
                }
            });
    }

    onStarClick(nextValue, prevValue, name) {
        this.setState({rating: nextValue});
    }

    render() {
        let react = this;
        const {rating} = this.state;
        return (
            <div className="modalLayout">
                <form onSubmit={react.handleSubmit}>
                    <Modal.Body>
                        {react.state.errors.length > 0 ? <Error errors={react.state.errors}/> : ''}

                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>:</label>
                                    <input type="text" value={react.state.salary.basic_salary || ''}
                                           onChange={react.handleBasicSalary} className="form-control"/>
                                </div>
                            </div>
                        </div>


                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Comment:</label>
                                    <input type="text" value={react.state.salary.basic_salary || ''}
                                           onChange={react.handleBasicSalary} className="form-control"/>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Rating: {rating}</label>
                                    <StarRating
                                        name="rate1"
                                        starCount={10}
                                        value={rating}
                                        onStarClick={this.onStarClick.bind(this)}
                                        className={'form-control'}
                                    />
                                </div>
                            </div>
                        </div>

                        <br/>
                    </Modal.Body>
                    <Modal.Footer>
                        <button className="btn btn-primary">Save</button>
                        <button type="button" className='btn btn-default' onClick={react.props.closeModal}>Cancel
                        </button>
                    </Modal.Footer>
                </form>
            </div>
        )
    }
}

export default CreateModal