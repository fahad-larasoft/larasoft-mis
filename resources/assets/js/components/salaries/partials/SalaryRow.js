import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import Modal from 'react-bootstrap-modal';

import axios from 'axios';
import PayModal from "./PayModal";
import EditModal from "./EditModal";

class SalaryRow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modelIsOpen: false
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    openModal() {
        this.setState({
            modalIsOpen: true
        })
    }

    closeModal() {
        this.setState({
            modalIsOpen: false
        })
    }

    handleTotal(){
        let salary = this.props.obj;
        return parseInt(salary.basic_salary || 0) + parseInt(salary.bonus_amount || 0) + parseInt(salary.overtime_amount || 0) - parseInt(salary.deduction_amount || 0);
    }

    handleSubmit(event) {
        event.preventDefault();
        const {history} = this.props;

        let uri = '/users/' + this.props.obj.id;
        axios.delete(uri);
        history.push('/users');
    }

    render() {
        let react = this;
        const deleteButton = {
            marginLeft: 3
        };

        return (
            <tr>
                <td>
                    {this.props.obj.basic_salary}
                </td>
                <td>
                    {this.props.obj.bonus_amount}
                </td>
                <td>
                    {this.props.obj.deduction_amount}
                </td>
                <td>
                    {this.props.obj.overtime_amount}
                </td>
                <td>
                    {this.props.obj.paying_date}
                </td>
                <td>
                    {this.props.obj.status.toUpperCase()}
                </td>
                <td>
                    {this.handleTotal()}
                </td>
                <td>

                    <div>
                        {react.props.is_admin ?
                            <button type="button" className="btn btn-info" onClick={this.openModal}> Edit
                            </button> : ''
                        }
                        &nbsp;&nbsp;
                        <Modal
                            show={this.state.modalIsOpen}
                            onHide={this.closeModal}
                            aria-labelledby="ModalHeader"
                        >
                            <Modal.Header>
                                <Modal.Title id='ModalHeader'>Pay Salary {this.props.obj.name}</Modal.Title>
                            </Modal.Header>
                            <PayModal openModal={this.openModal} closeModal={this.closeModal}
                                      handleUpdate={this.props.handleUpdate}
                                      salary={this.props.obj}
                                      handleMessagesUpdate={react.props.handleMessagesUpdate}
                            />
                        </Modal>
                    </div>

                </td>
            </tr>
        );
    }
}


export default SalaryRow;