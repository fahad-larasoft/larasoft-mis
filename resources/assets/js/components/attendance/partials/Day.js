import React from 'react';
import propTypes from 'prop-types';
import moment from 'moment';

class Day extends React.Component {
    render() {
        let react = this;
        let is_absent = false;
        for (let i in react.props.holidays){
            let absent_day_string = moment(react.props.holidays[i].start).format('ddd');
            if (absent_day_string === react.props.day.dayString){
                is_absent = true;
                break;
            }
        }

        return (
            <div>{
                is_absent ? <span>absent</span> :
                    <i><i onClick={() => react.props.handleAbsent(react.props.user, react.props.day)}
                          className="btn btn-primary fa fa-bandcamp"/></i>}
            </div>)
    };
}

Day.propTypes = {
    holidays: propTypes.array.isRequired,
    day: propTypes.object.isRequired
};

export default Day