import React from 'react';
import propTypes from 'prop-types';
import Day from "./Day";

const StaffList = props => {
    return (
        <tr>
            <td>{props.user.id}</td>
            <td>{props.user.name}</td>
            {props.week.map((day, index) =>
                <td key={index}>
                    <Day user={props.user} handleAbsent={props.handleAbsent} day={day} holidays={props.user.holidays}/>
                    </td>
            )}
        </tr>
    )
};

StaffList.propTypes = {
    user: propTypes.object.isRequired
};

export default StaffList