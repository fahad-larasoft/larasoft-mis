import React, {Component} from 'react';
import axios from 'axios';
import StaffList from "./partials/StaffList";
import moment from 'moment';

class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
            week: this.calculateWeekdays()
        };
    }



    componentDidMount() {
        this.calculateWeekdays();
        this.fetchUser();
    }

    calculateWeekdays = () => {
        let week = [];
        let first_day = moment().startOf('isoWeek');

        for (let i = 0; i<7; i++){
            let copyOfFirstDay = moment(first_day);
            let currentDay = copyOfFirstDay.add(i, 'days');
            let day = {
                date: currentDay,
                dayString: currentDay.format('ddd')
            };
            week.push(day);
        }
        return week;
    };

    fetchUser = () => {
        let react = this;
        let params = {
            from: react.state.week[0].date.format('YYYY-MM-DD'),
            to: react.state.week[6].date.format('YYYY-MM-DD'),
        };
        axios.get('/admin/holidays/users', { params: params })
            .then(response => {
                console.log(response.data.data);
                react.setState({
                    users: response.data.data
                })
            })
            .catch(error => {
                console.log(error);
            })
    };

    handleAbsent = (user, day) => {
        let holiday = {
            user_id: user.id,
            title: 'absent marked by admin',
            start: day.date.format('YYYY-MM-DD'),
            status: 'absent',
        };

        axios.post('admin/absent', holiday)
            .then(response => {
                this.fetchUser();
            })
            .catch(error => {
                console.log(error)
            })
    };

    render() {
        let react = this;
        let firstDayOfWeek = react.state.week[0].date.format('YYYY-MM-DD');
        let lastDayOfWeek = react.state.week[6].date.format('YYYY-MM-DD');
        return (
            <div className="box">
                <div className="box-header with-border">
                    <div className="row">
                        <div className="col-md-4"><h3 className="box-title">Attendance</h3></div>
                        <div className="col-md-4">Week: [{firstDayOfWeek} -- {lastDayOfWeek}]</div>
                        <div className="col-md-4"/>
                    </div>

                </div>
                <div className="box-body">
                    <table className="table table-bordered">
                        <thead>
                        <tr>
                            <th style={{'width': '10px'}}>#</th>
                            <th>Name</th>
                            {react.state.week.map((day, index) =>
                                <th key={index}>{day.dayString}</th>
                            )}
                        </tr>
                        </thead>

                        <tbody>
                        {react.state.users.map((user, index) =>
                             <StaffList handleAbsent={react.handleAbsent} week={react.state.week} key={index} user={user}/>
                        )}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

export default Index