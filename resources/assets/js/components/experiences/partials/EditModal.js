import React, {Component} from 'react';
import Modal from 'react-bootstrap-modal';
import moment from 'moment';
import DatePicker from 'react-datepicker';

import 'react-datepicker/dist/react-datepicker.css';
import Error from "../../common/Error";

class EditModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            experience: {
                job_title: '',
                description: '',
                company_name: '',
                company_phone_number: '',
            },
            start_date: moment(),
            end_date: moment(),
            errors: [],
            messages: [],
            open: false,
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleJobTitle = this.handleJobTitle.bind(this);
        this.handleDescription = this.handleDescription.bind(this);
        this.handleStartDate = this.handleStartDate.bind(this);
        this.handleEndDate = this.handleEndDate.bind(this);
        this.handleCompanyName = this.handleCompanyName.bind(this);
        this.handleCompanyPhoneNumber = this.handleCompanyPhoneNumber.bind(this);
    }

    componentDidMount(){
        let react = this;
        react.setState({
            experience: react.props.experience,
            start_date: moment(react.props.experience.start_date),
            end_date: react.props.experience.end_date ? moment(react.props.experience.end_date) : null
        })
    }

    handleJobTitle(e){
        let experience = this.state.experience;
        experience.job_title = e.target.value;
        this.setState({
            experience: experience
        })
    }

    handleDescription(e){
        let experience = this.state.experience;
        experience.description = e.target.value;
        this.setState({
            experience: experience
        })
    }

    handleStartDate(date){
        this.setState({
            start_date: date
        })
    }

    handleEndDate(date){
        this.setState({
            end_date: date
        })
    }

    handleCompanyName(e){
        let experience = this.state.experience;
        experience.company_name = e.target.value;
        this.setState({
            experience: experience
        })
    }

    handleCompanyPhoneNumber(e){
        let experience = this.state.experience;
        experience.company_phone_number = e.target.value;
        this.setState({
            experience: experience
        })
    }

    handleSubmit(e) {
        let react = this;
        e.preventDefault();
        const { history } = this.props;

        let statusUpdate = {
            create: false,
            update: true,
            delete: false,
        };

        let experience = react.state.experience;
        experience.start_date = react.state.start_date ? react.state.start_date.format('YYYY-MM-DD') : null;
        experience.end_date = react.state.end_date ? react.state.end_date.format('YYYY-MM-DD') : null;

        axios.put(`user/experiences/${this.props.experience.id}`, experience)
            .then(response => {
                react.props.handleUpdateEditModel(response.data.data, statusUpdate);
                react.props.handleMessagesUpdate(response.data.message);
                this.props.closeModal();
            })
            .catch(function (error) {
                console.log(error.response.data.errors);
                if (error.response) {
                    react.setState({ errors: error.response.data.errors });
                    setTimeout(function(){
                        react.setState({ errors: [] });
                    }, 4000);
                }
            });

    }

    render() {
        let react = this;
        return (
            <div className="modalLayout">
                <form onSubmit={ this.handleSubmit }>
                    <Modal.Body>
                        {react.state.errors.length > 0 ? <Error errors={react.state.errors}/> : ''}

                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Job Title:</label>
                                    <input type="text" value={react.state.experience.job_title || ''} onChange={ react.handleJobTitle } className="form-control" />
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Start Date:</label>
                                    <DatePicker  selected={ react.state.start_date }
                                                 className="form-control"
                                                 onChange={ react.handleStartDate }/>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>End Date:</label>
                                    <DatePicker  selected={ react.state.end_date }
                                                 className="form-control"
                                                 onChange={ react.handleEndDate }/>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>description:</label>
                                    <textarea value={react.state.experience.description || ''} onChange={ react.handleDescription } className="form-control" />
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Company Name:</label>
                                    <input type="text" value={ react.state.experience.company_name || '' } onChange={ react.handleCompanyName } className="form-control" />
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Company Phone Number:</label>
                                    <input type="text" value={ react.state.experience.company_phone_number || '' } onChange={ react.handleCompanyPhoneNumber } className="form-control" />
                                </div>
                            </div>
                        </div>

                        <br />
                    </Modal.Body>
                    <Modal.Footer>
                        <button className="btn btn-primary">Update</button>
                        <button type="button" className='btn btn-default' onClick={ react.props.closeModal }>Cancel</button>
                    </Modal.Footer>
                </form>
            </div>
        )
    }
}

export default EditModal