import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import Modal from 'react-bootstrap-modal';
import EditModal from './EditModal';
import ConfirmDelete from "../../common/ConfirmDelete";

class ExperienceRow extends Component{
    constructor(props){
        super(props);
        this.state = {
            experience: props.experience,
            modalIsOpen: false,
            deleteModalIsOpen: false
        };

        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.openDeleteModal = this.openDeleteModal.bind(this);
        this.closeDeleteModal = this.closeDeleteModal.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    openModal(){
        this.setState({
            modalIsOpen: true
        })
    }

    closeModal(){
        this.setState({
            modalIsOpen: false
        })
    }

    openDeleteModal(){
        this.setState({
            deleteModalIsOpen: true
        })
    }

    closeDeleteModal(){
        this.setState({
            deleteModalIsOpen: false
        })
    }

    handleDelete(){
        let react = this;
        let statusUpdate = {
            create: false,
            update: false,
            delete: true,
        };

        axios.delete(`user/experiences/${this.state.experience.id}`)
            .then(response => {
                react.props.handleUpdate(react.state.experience, statusUpdate);
                console.log(response);
            })
            .catch(error => {
                console.log(error)
            })
    }

    render(){
        let react = this;
        return (
            <div className="post">
                <br/>
                <hr/>
                <button type="button" className="pull-right btn-danger" onClick={ react.openDeleteModal }><i className="fa fa-trash"/> Delete</button>
                <Modal
                    show={ react.state.deleteModalIsOpen }
                    onHide={react.closeDeleteModal }
                    aria-labelledby="ModalHeader"
                >
                    <Modal.Header>
                        <Modal.Title id='ModalHeader'>Delete {react.state.experience.job_title}</Modal.Title>
                    </Modal.Header>
                    <ConfirmDelete
                        openModal={ react.openDeleteModal }
                        closeModal={ react.closeDeleteModal }
                        title={ react.state.experience.job_title }
                        handleDelete={ react.handleDelete }
                    />
                </Modal>
                <button type="button" className="pull-right btn-info" onClick={ this.openModal }><i className="fa fa-edit"/> Edit</button>
                    <Modal
                        show={ react.state.modalIsOpen }
                        onHide={react.closeModal }
                        aria-labelledby="ModalHeader"
                        backdrop="static"
                        keyboard= {false}
                    >
                        <Modal.Header>
                            <Modal.Title id='ModalHeader'>Edit {react.state.experience.job_title}</Modal.Title>
                        </Modal.Header>
                        <EditModal
                            openModal={ react.openModal }
                            closeModal={ react.closeModal }
                            handleUpdateEditModel={react.props.handleUpdate}
                            experience={react.state.experience}
                            handleMessagesUpdate={react.props.handleMessagesUpdate}
                        />
                    </Modal>

                <div className="user-block">
                    <img className="img-circle img-bordered-sm" src={ react.state.experience.company_logo } alt="user image"/>
                    <span className="username">
                          <Link to={ react.state.experience.company_url }>{ react.state.experience.company_name }</Link>
                        </span>
                    <span className="description">from { react.state.experience.start_date } - to { react.state.experience.end_date ? this.state.experience.end_date : 'present' }</span>
                </div>
                <p>
                    {react.state.experience.description}
                </p>
                <ul className="list-inline">
                    <li><Link to="#" className="link-black text-sm"><i className="fa fa-phone margin-r-5"/> Company Tel: {react.state.experience.company_phone_number }</Link></li>
                    <li><Link to="#" className="link-black text-sm"><i className="fa fa-address-card margin-r-5"/> Address: </Link>
                    </li>
                    <li className="pull-right">
                        <span className="description">Job title: <strong>{ react.state.experience.job_title }</strong></span>
                    </li>
                </ul>
            </div>
        )
    }
}

export  default ExperienceRow