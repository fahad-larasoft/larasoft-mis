import React, {Component} from 'react';
import Model from 'react-bootstrap-modal';
import ExperienceRow from './partials/ExperienceRow';
import CreateModal from "./partials/CreateModal";
import axios from 'axios';

class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            experiences: [],
            messages: [],
            modalIsOpen: false,
        };

        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.handleToUpdate = this.handleToUpdate.bind(this);
    }

    openModal() {
        this.setState({modalIsOpen: true})
    }

    closeModal() {
        this.setState({modalIsOpen: false})
    }

    handleToUpdate(experience, statusUpdate) {
        let experiences = this.state.experiences;

        if (statusUpdate.delete) {
            for (let i = 0; i < experiences.length; i++) {
                if (experiences[i].id === experience.id) {
                    if (i > -1) {
                        this.setState({
                            experiences: []
                        });
                        experiences.splice(i, 1);
                    }
                }
            }
        }
        if (!statusUpdate.delete) {
            for (let i = 0; i < experiences.length; i++) {
                if (experiences[i].id === experience.id) {
                    if (i > -1) {
                        this.setState({
                            experiences: []
                        });
                        experiences.splice(i, 1);
                    }
                }
            }

            experiences.push(experience);
        }
        this.setState({
            experiences: experiences
        });
    }

    componentDidMount() {
        axios.get('user/experiences')
            .then(response => {
                this.setState({experiences: response.data.data});
            })
            .catch(error => {
                console.log(error)
            })
    }

    experienceRow() {
        let react = this;
        let experiences = this.state.experiences;
        if (experiences instanceof Array) {
            if (experiences.length > 0) {
                return experiences.map(function (experience, i) {
                    return <ExperienceRow
                        experience={experience} key={i}
                        modalIsOpen={react.state.modalIsOpen}
                        openModal={react.openModal}
                        closeModal={react.closeModal}
                        handleUpdate={react.handleToUpdate}
                        handleMessagesUpdate={react.props.handleMessagesUpdate}
                    />;
                })
            } else {
                return (
                    <div className="active tab-pane" id="experiences">
                        <div>No Experience</div>
                    </div>
                )
            }

        }
    }

    render() {
        let react = this;
        return (
            <div className="active tab-pane" id="experiences">
                <button type="button" className="pull-right btn-default" onClick={this.openModal}><i
                    className="fa fa-plus"/> Create
                </button>
                <Model
                    show={react.state.modalIsOpen}
                    onHide={react.closeModal}
                    aria-labelledby="ModalHeader"
                >
                    <CreateModal
                        openModal={react.openModal}
                        closeModal={react.closeModal}
                        handleUpdate={react.handleToUpdate}
                        handleMessagesUpdate={react.props.handleMessagesUpdate}
                    />
                </Model>
                {this.experienceRow()}
            </div>
        )
    }
}

export default Index
