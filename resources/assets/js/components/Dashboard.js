import React, {Component} from 'react';
import fullCalendar from 'fullcalendar';
import 'fullcalendar/dist/fullcalendar.css';
import Modal from 'react-bootstrap-modal';
import CreateModal from './holidays/partials/CreateModal';
import AdminEditModal from './holidays/partials/AdminEditModal';
import UserEditModal from './holidays/partials/UserEditModal';
import axios from 'axios';
import Success from "./common/Success";
import Error from "./common/Error";
import moment from 'moment';

class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            holidays: [],
            modalIsOpen: false,
            adminEditModalIsOpen: false,
            userEditModalIsOpen: false,
            holiday: {},
            messages: [],
            errors: [],
            current_event: '',
            calendar_events: []
        };

        this.instantiateCalender = this.instantiateCalender.bind(this);
        this.handleHolidayUpdate = this.handleHolidayUpdate.bind(this);
        this.handleSubmitUpdate = this.handleSubmitUpdate.bind(this);
        this.handleMessageUpdate = this.handleMessageUpdate.bind(this);
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.openAdminEditModal = this.openAdminEditModal.bind(this);
        this.openUserEditModal = this.openUserEditModal.bind(this);
        this.closeAdminEditModal = this.closeAdminEditModal.bind(this);
        this.closeUserEditModal = this.closeUserEditModal.bind(this);
    }

    componentDidMount() {
        let react = this;
        axios.get(react.generateUrl())
            .then(response => {
                react.setState({
                    holidays: response.data.data
                });
                react.fetchPublicHolidays();
            });

    }

    fetchPublicHolidays(){
        let react = this;
        let apiKey = window.App.google_cal_api_key;

        if (apiKey) {
            let url = `https://www.googleapis.com/calendar/v3/calendars/en.pk%23holiday%40group.v.calendar.google.com/events?key=${apiKey}`;
            axios.get(url)
                .then(response => {
                    let holidays = react.state.holidays;

                    response.data.items.map(function (item) {
                        // console.log(item);
                        let holiday = {
                            id: item.id,
                            title: item.summary,
                            status: 'approved',
                            by: 'Public',
                            start: item.start.date,
                            end: item.end.date,
                        };
                        holidays.push(holiday);
                    });

                    react.setState({
                        holidays: holidays
                    });

                    react.instantiateCalender();
                })
                .catch(error => {

                })
        }else {
            react.instantiateCalender();
        }
    }

    openModal() {
        this.setState({
            modalIsOpen: true
        })
    }

    closeModal() {
        this.setState({
            modalIsOpen: false
        })
    }

    openAdminEditModal() {
        this.setState({
            adminEditModalIsOpen: true
        })
    }

    closeAdminEditModal() {
        this.setState({
            adminEditModalIsOpen: false
        })
    }

    openUserEditModal() {
        this.setState({
            userEditModalIsOpen: true
        })
    }

    closeUserEditModal() {
        this.setState({
            userEditModalIsOpen: false
        })
    }

    generateUrl(){
        let react = this;
        let isAdmin = window.App.is_admin;
        return isAdmin ? 'admin/holidays': 'user/holidays';
    }

    instantiateCalender() {
        let react = this;

        if (window.App.is_admin){
            $('#calendar').fullCalendar({
                firstDay: 1,
                events: react.state.holidays,
                eventRender: function(event, element) {
                    element.find('.fc-title').text(event.by + "-" + event.title);
                },
                eventClick: function (event) {
                    react.setState({
                        holiday: event,
                    });
                    react.openAdminEditModal();
                },

                dayClick: function (e) {
                    let calendar_events = $('#calendar').fullCalendar('clientEvents');
                    let isEvent = react.validateCalendar(e, calendar_events);

                    if (isEvent){
                        react.setState({ errors: ['You cannot apply for this holiday.'] });

                        setTimeout(function(){
                            react.setState({ errors: [] });
                        }, 10000);
                    }else {
                        react.setState({
                            current_event: e,
                            calendar_events: calendar_events,
                        });
                        react.openModal();
                    }
                }
            });
        }else {
            $('#calendar').fullCalendar({
                events: react.state.holidays,
                eventRender: function(event, element) {
                    element.find('.fc-title').text(event.by + "-" + event.title);
                },
                dayClick: function (e) {
                    let calendar_events = $('#calendar').fullCalendar('clientEvents');
                    let isEvent = react.validateCalendar(e, calendar_events);

                    if (isEvent){
                        react.setState({ errors: ['You cannot apply for this holiday.'] });

                        setTimeout(function(){
                            react.setState({ errors: [] });
                        }, 10000);
                    }else {
                        react.setState({
                            current_event: e,
                            calendar_events: calendar_events,
                        });
                        react.openModal();
                    }
                },

                eventClick: function (event) {
                    let calendar_events = $('#calendar').fullCalendar('clientEvents');
                    if (window.App.auth_user.id === event.user_id && event.status === 'pending'){
                        react.setState({
                            holiday: event,
                            calendar_events: calendar_events,
                        });
                        react.openUserEditModal();
                    }
                },
            });
        }
    }

    validateCalendar(selected_date, events){
        let react = this;
        for (let i in events) {
            if (events[i].start.isSame(selected_date)) {
                if (window.App.is_admin){
                    console.log();
                    if( events[i].user_id !== window.App.admin.id ){
                        console.log('here');
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }


    handleSubmitUpdate(event) {
        let react = this;
        let status = prompt('Enter approved or rejected');

        let statusUpdate = {
            create: false,
            update: true,
            delete: false,
        };

        let holiday = {
            user_id: window.App.auth_user.id,
            title: event.title,
            start: event.start.format('YYYY-MM-DD'),
            status: status,
        };

        axios.put(`admin/holidays/${event.id}`, holiday)
            .then(response => {
                react.handleHolidayUpdate(response.data.data, statusUpdate);
            })
            .catch(error => {
                console.log(error);
            })
    }

    handleHolidayUpdate(holiday, statusUpdate) {
        let react = this;
        let holidays = react.state.holidays;

        for (let i = 0; i < holidays.length; i++) {
            if (holidays[i].id === holiday.id) {
                if (i > -1) {
                    holidays.splice(i, 1);
                }
            }
        }

        if (!statusUpdate.delete) {
            holidays.push(holiday);
        }

        react.setState({
            holidays: holidays
        });

        $.each($('#calendar').fullCalendar('clientEvents'), function (i, item) {
            $('#calendar').fullCalendar('removeEvents', item.id);
        }); // removes the event one by one

        $('#calendar').fullCalendar('addEventSource', holidays);
    }

    handleMessageUpdate(message) {
        let react = this;
        // let messages = react.state.messages;
        // messages.push(message);
        react.setState({
            messages: [message]
        });

        setTimeout(function () {
            react.setState({messages: []});
        }, 10000);
    }

    render() {
        let react = this;
        return (
            <div>
                    {react.state.messages.length > 0 ? <Success messages={react.state.messages}/> : ''}
                    {react.state.errors.length > 0 ? <Error errors={react.state.errors}/> : ''}
                <div>
                    <div style={{'backgroundColor': 'white'}}>
                        <div id="calendar"/>
                    </div>

                    <Modal
                        show={react.state.modalIsOpen}
                        onHide={react.closeModal}
                        aria-labelledby="ModalHeader"
                        backdrop="static"
                        keyboard={false}
                    >
                        <Modal.Header>
                            <Modal.Title id='ModalHeader'>Request a leave</Modal.Title>
                        </Modal.Header>
                        <CreateModal
                            openModal={react.openModal}
                            closeModal={react.closeModal}
                            handleUpdate={react.handleHolidayUpdate}
                            selected_start_date={react.state.current_event}
                            date={react.state.current_event}
                            calendar_events={react.state.calendar_events}
                            handleMessagesUpdate={react.handleMessageUpdate}
                        />
                    </Modal>

                    <Modal
                        show={react.state.adminEditModalIsOpen}
                        onHide={react.closeAdminEditModal}
                        aria-labelledby="ModalHeader"
                        backdrop="static"
                        keyboard={false}
                    >
                        <Modal.Header>
                            <Modal.Title id='ModalHeader'>Edit holiday - Admin</Modal.Title>
                        </Modal.Header>
                        <AdminEditModal
                            openModal={react.openAdminEditModal}
                            closeModal={react.closeAdminEditModal}
                            handleUpdate={react.handleHolidayUpdate}
                            holiday={react.state.holiday}
                            handleMessagesUpdate={react.handleMessageUpdate}
                            calendar_events={react.state.calendar_events}
                        />
                    </Modal>

                    <Modal
                        show={react.state.userEditModalIsOpen}
                        onHide={react.closeAdminEditModal}
                        aria-labelledby="ModalHeader"
                        backdrop="static"
                        keyboard={false}
                    >
                        <Modal.Header>
                            <Modal.Title id='ModalHeader'>Edit holiday - User</Modal.Title>
                        </Modal.Header>
                        <UserEditModal
                            openModal={react.openUserEditModal}
                            closeModal={react.closeUserEditModal}
                            handleUpdate={react.handleHolidayUpdate}
                            holiday={react.state.holiday}
                            date={react.state.holiday.start}
                            handleMessagesUpdate={react.handleMessageUpdate}
                            calendar_events={react.state.calendar_events}
                            selected_start_date={react.state.holiday.start}
                        />
                    </Modal>
                </div>
            </div>
        );
    }
}

export default Dashboard;

