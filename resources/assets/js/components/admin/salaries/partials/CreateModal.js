import React, {Component} from 'react';
import Modal from 'react-bootstrap-modal';
import axios from 'axios';

import 'react-datepicker/dist/react-datepicker.css';
import Error from "../../../common/Error";

class CreateModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            salary: {
                basic_salary: '',
                bonus_amount: '',
                deduction_amount: '',
                overtime_amount: '',
                staff_id: '',
                status: 'pending',
            },
            staffs: [],
            errors: [],
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleBasicSalary = this.handleBasicSalary.bind(this);
        this.handleBonusAmount = this.handleBonusAmount.bind(this);
        this.handleDeductionAmount = this.handleDeductionAmount.bind(this);
        this.handleOvertimeAmount = this.handleOvertimeAmount.bind(this);
        this.handleStaff = this.handleStaff.bind(this);
    }

    componentDidMount() {
        let react = this;
        axios.get('admin/staffs')
            .then(response => {
                react.setState({
                    staffs: response.data.data,
                });
            })
            .catch(error => {
                console.log(error);
            })
    }

    handleStaff(e) {
        let salary = this.state.salary;
        salary.staff_id = e.target.value;
        let staff = this.state.staffs.filter(function (obj) {
            console.log(obj.id);
            console.log(e.target.value);
            if (obj.id == e.target.value){
                return obj;
            }
        });

        salary.basic_salary = staff[0].basic_pay;
        this.setState({
            salary: salary
        })
    }

    handleBasicSalary(e) {
        let salary = this.state.salary;
        salary.basic_salary = e.target.value;
        this.setState({
            salary: salary
        })
    }

    handleBonusAmount(e) {
        let salary = this.state.salary;
        salary.bonus_amount = e.target.value;
        this.setState({
            salary: salary
        })
    }

    handleOvertimeAmount(e) {
        let salary = this.state.salary;
        salary.overtime_amount = e.target.value;
        this.setState({
            salary: salary
        })
    }

    handleDeductionAmount(e) {
        let salary = this.state.salary;
        salary.deduction_amount = e.target.value;
        this.setState({
            salary: salary
        })
    }

    handleSubmit(e) {
        let react = this;
        e.preventDefault();

        let statusUpdate = {
            create: false,
            update: true,
            delete: false,
        };

        let salary = react.state.salary;

        axios.post('admin/salaries', salary)
            .then(response => {
                react.props.handleUpdate(response.data.data, statusUpdate);
                react.props.handleMessagesUpdate(response.data.message);
                this.props.closeModal();
            })
            .catch(function (error) {
                console.log(error.response.data.errors);
                if (error.response) {
                    react.setState({errors: error.response.data.errors});
                    setTimeout(function () {
                        react.setState({errors: []});
                    }, 4000);
                }
            });

    }

    render() {
        let react = this;
        return (
            <div className="modalLayout">
                <form onSubmit={react.handleSubmit}>
                    <Modal.Body>
                        {react.state.errors.length > 0 ? <Error errors={react.state.errors}/> : ''}

                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <select className="form-control" value={react.state.salary.staff_id} onChange={react.handleStaff}>
                                        <option>--Select staff--</option>
                                        {react.state.staffs.map(function (staff) {
                                            return (
                                                <option key={staff.id} value={staff.id}>{staff.user.name}</option>
                                            )
                                        })}
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Basic Pay:</label>
                                    <input type="text" value={react.state.salary.basic_salary || ''}
                                           onChange={react.handleBasicSalary} readOnly={true} className="form-control"/>
                                </div>
                            </div>
                        </div>


                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Bonus (pkr):</label>
                                    <textarea value={react.state.salary.bonus_amount} onChange={react.handleBonusAmount}
                                              className="form-control"/>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Deduction (pkr):</label>
                                    <input type="text" value={react.state.salary.deduction_amount}
                                           onChange={react.handleDeductionAmount} className="form-control"/>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Overtime (pkr):</label>
                                    <input type="text" value={react.state.salary.overtime_amount}
                                           onChange={react.handleOvertimeAmount} className="form-control"/>
                                </div>
                            </div>
                        </div>

                        <br/>
                    </Modal.Body>
                    <Modal.Footer>
                        <button className="btn btn-primary">Save</button>
                        <button type="button" className='btn btn-default' onClick={react.props.closeModal}>Cancel
                        </button>
                    </Modal.Footer>
                </form>
            </div>
        )
    }
}

export default CreateModal