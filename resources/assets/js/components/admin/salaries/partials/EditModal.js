import React, {Component} from 'react';
import Modal from 'react-bootstrap-modal';
import moment from 'moment';
import DatePicker from 'react-datepicker';

import 'react-datepicker/dist/react-datepicker.css';
import Error from "../../../common/Error";

class EditModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            qualification: {},
            start_date: moment(),
            passing_year: moment(),
            errors: [],
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDegreeTitle = this.handleDegreeTitle.bind(this);
        this.handleInstitute = this.handleInstitute.bind(this);
        this.handleStartDate = this.handleStartDate.bind(this);
        this.handlePassingDate = this.handlePassingDate.bind(this);
        this.handleMajor = this.handleMajor.bind(this);
    }

    componentDidMount(){
        let react = this;
        react.setState({
            qualification: react.props.qualification,
        })
    }

    handleDegreeTitle(e){
        let qualification = this.state.qualification;
        qualification.degree_title = e.target.value;
        this.setState({
            qualification: qualification
        })
    }

    handleInstitute(e){
        let qualification = this.state.qualification;
        qualification.institute = e.target.value;
        this.setState({
            qualification: qualification
        })
    }

    handleStartDate(date){
        this.setState({
            start_date: date
        })
    }

    handlePassingDate(date){
        this.setState({
            passing_year: date
        })
    }

    handleMajor(e){
        let qualification = this.state.qualification;
        qualification.major = e.target.value;
        this.setState({
            qualification: qualification
        })
    }

    handleSubmit(e) {
        let react = this;
        e.preventDefault();
        const { history } = this.props;

        let statusUpdate = {
            create: false,
            update: true,
            delete: false,
        };

        let qualification = react.state.qualification;
        qualification.start_date = react.state.start_date.format('YYYY-MM-DD');
        qualification.passing_year = react.state.passing_year.format('YYYY-MM-DD');

        axios.put(`user/qualifications/${qualification.id}`, qualification)
            .then(response => {
                react.props.handleUpdateEditModel(response.data.data, statusUpdate);
                this.props.closeModal();
            })
            .catch(function (error) {
                console.log(error.response.data.errors);
                if (error.response) {
                    react.setState({ errors: error.response.data.errors });
                    setTimeout(function(){
                        react.setState({ errors: [] });
                    }, 4000);
                }
            });
    }

    render() {
        let react = this;
        return (
            <div className="modalLayout">
                <form onSubmit={ react.handleSubmit }>
                    <Modal.Body>
                        {react.state.errors.length > 0 ? <Error errors={react.state.errors}/> : ''}

                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Degree Title:</label>
                                    <input type="text" value={react.state.qualification.degree_title} onChange={ react.handleDegreeTitle } className="form-control" />
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Start Date:</label>
                                    <DatePicker  selected={ react.state.start_date }
                                                 onChange={ react.handleStartDate }/>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Passing Date:</label>
                                    <DatePicker  selected={ react.state.passing_year }
                                                 onChange={ react.handlePassingDate }/>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>institute:</label>
                                    <textarea value={react.state.qualification.institute} onChange={ react.handleInstitute } className="form-control" />
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Major:</label>
                                    <input type="text" value={ react.state.qualification.major } onChange={ react.handleMajor } className="form-control" />
                                </div>
                            </div>
                        </div>

                        <br />
                    </Modal.Body>
                    <Modal.Footer>
                        <button className="btn btn-primary">Update</button>
                        <button type="button" className='btn btn-default' onClick={ react.props.closeModal }>Cancel</button>
                    </Modal.Footer>
                </form>
            </div>
        )
    }
}

export default EditModal