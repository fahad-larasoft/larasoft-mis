import React, {Component} from 'react';
import Modal from 'react-bootstrap-modal';

import axios from 'axios';
import PayModal from "./PayModal";

class SalaryRow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modelIsOpen: false
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    openModal() {
        this.setState({
            modalIsOpen: true
        })
    }

    closeModal() {
        this.setState({
            modalIsOpen: false
        })
    }

    handleTotal(){
        let salary = this.props.obj;
        return parseInt(salary.basic_salary || 0) + parseInt(salary.bonus_amount || 0) + parseInt(salary.overtime_amount || 0) - parseInt(salary.deduction_amount || 0);
    }

    handleSubmit(event) {
        event.preventDefault();
        const {history} = this.props;

        let uri = '/users/' + this.props.obj.id;
        axios.delete(uri);
        history.push('/users');
    }

    render() {
        let react = this;

        return (
            <tr>
                <td>
                    {react.props.obj.basic_salary}
                </td>
                <td>
                    {react.props.obj.bonus_amount}
                </td>
                <td>
                    {react.props.obj.deduction_amount}
                </td>
                <td>
                    {react.props.obj.overtime_amount}
                </td>
                <td>
                    {react.props.obj.paying_date}
                </td>
                <td>
                    {react.props.obj.status.toUpperCase()}
                </td>
                <td>
                    {react.handleTotal()}
                </td>
                <td>
                    {react.props.obj.staff.user.name}
                </td>
                <td>

                    <div>
                            <button type="button" className="btn btn-info" onClick={react.openModal}> Edit
                            </button>
                        &nbsp;&nbsp;
                        <Modal
                            show={react.state.modalIsOpen}
                            onHide={react.closeModal}
                            aria-labelledby="ModalHeader"
                        >
                            <Modal.Header>
                                <Modal.Title id='ModalHeader'>Pay Salary {react.props.obj.name}</Modal.Title>
                            </Modal.Header>
                            <PayModal openModal={react.openModal} closeModal={react.closeModal}
                                      handleUpdate={react.props.handleUpdate}
                                      salary={react.props.obj}
                                      handleMessagesUpdate={react.props.handleMessagesUpdate}
                            />
                        </Modal>
                    </div>

                </td>
            </tr>
        );
    }
}


export default SalaryRow;