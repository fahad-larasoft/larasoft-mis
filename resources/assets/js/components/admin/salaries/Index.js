import React, {Component} from 'react';
import TableRow from './partials/SalaryRow';
import Modal from 'react-bootstrap-modal';
import CreateModal from './partials/CreateModal';
import axios from 'axios'
import Success from "../../common/Success";

class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            salaries: [],
            messages: [],
            modalIsOpen: false,
        };

        this.handleSalaryUpdate = this.handleSalaryUpdate.bind(this);
        this.handleMessageUpdate = this.handleMessageUpdate.bind(this);
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    openModal(){
        this.setState({
            modalIsOpen: true
        })
    }

    closeModal(){
        this.setState({
            modalIsOpen: false
        })
    }

    componentDidMount() {
        let react = this;
        let url = `/admin/salaries`;

        axios.get(url)
            .then(response => {
                this.setState({
                    salaries: response.data.data,
                });
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    tabRow() {
        let react = this;
        if (react.state.salaries instanceof Array) {
            return react.state.salaries.map(function (object, i) {
                return <TableRow
                    obj={object}
                    key={i}
                    handleUpdate={react.handleSalaryUpdate}
                    handleMessagesUpdate={react.handleMessageUpdate}
                />;

            })
        }
    }

    handleSalaryUpdate(salary, statusUpdate){
        let salaries = this.state.salaries;
        if (!statusUpdate.delete) {
            for (let i = 0; i < salaries.length; i++) {
                if (salaries[i].id === salary.id) {
                    if (i > -1) {
                        salaries.splice(i, 1);
                    }
                }
            }

            salaries.push(salary);
        }

        this.setState({
            salaries: salaries
        })
    }

    handleMessageUpdate(message){
        let react = this;
        let messages = react.state.messages;
        messages.push(message);
        react.setState({
            messages: messages
        });

        setTimeout(function(){
            react.setState({ messages: [] });
        }, 10000);
    }

    render() {
        const createButton = {
            marginTop: -27
        };

        let react = this;
        return (
            <div>
                <div className="box-body no-padding">
                    {react.state.messages.length > 0 ? <Success messages={react.state.messages}/> : ''}

                    <div className="box">
                    <div className="box-header">Salaries
                        <div>
                                <button onClick={react.openModal} className="btn btn-default pull-right" style={createButton}><i className="fa fa-plus"/>&nbsp;
                                    Create</button>

                            <Modal
                                show={react.state.modalIsOpen}
                                onHide={react.closeModal}
                                aria-labelledby="ModalHeader"
                            >
                                <Modal.Header>
                                    <Modal.Title id='ModalHeader'>Pay Salary</Modal.Title>
                                </Modal.Header>
                                <CreateModal openModal={react.openModal} closeModal={react.closeModal}
                                             handleUpdate={react.handleSalaryUpdate}
                                             handleMessagesUpdate={react.handleMessageUpdate}
                                />
                            </Modal>
                        </div>
                    </div>
                        <table className="table table-striped">
                            <thead>
                            <tr>
                                <th>Basic Pay</th>
                                <th>Bonus (pkr)</th>
                                <th>Deduction (pkr)</th>
                                <th>Overtime (pkr)</th>
                                <th>Pay Date</th>
                                <th>Status</th>
                                <th>Total (pkr)</th>
                                <th>User</th>
                                <th width="200px">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            {react.tabRow()}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

export default Index

