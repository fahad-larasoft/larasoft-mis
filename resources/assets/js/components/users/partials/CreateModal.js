import React, {Component} from 'react';
import Modal from 'react-bootstrap-modal';

import 'react-datepicker/dist/react-datepicker.css';
import Error from "../../common/Error";

class CreateModal extends Component {
    constructor(props){
        super(props);
        this.state = {userName: '', userEmail: '', errors: []};

        this.handleUserName = this.handleUserName.bind(this);
        this.handleUserEmail = this.handleUserEmail.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleUserName(e){
        this.setState({
            userName: e.target.value
        })
    }
    handleUserEmail(e){
        this.setState({
            userEmail: e.target.value
        })
    }

    handleSubmit(e){
        let react = this;
        e.preventDefault();
        const user = {
            name: this.state.userName,
            email: this.state.userEmail,
        };

        axios.post('invite', user)
            .then(function (response) {
                console.log(response);
                react.props.handleMessagesUpdate(response.data.message);
                react.props.closeModal();
            })

            .catch(function (error) {
                if (error.response) {
                    var validationErrors = [];
                    $.each(error.response.data, function(key, value1){
                        $.each(value1, function(key, value2){
                            validationErrors.push(value2)
                        });
                    });
                    console.log(validationErrors);
                    react.setState({ errors: validationErrors });

                    setTimeout(function(){
                        react.setState({ errors: [] });
                    }, 4000);
                }
            });
    }

    render() {
        let react = this;
        return (
            <div className="modalLayout">
                <form onSubmit={ react.handleSubmit }>
                    <Modal.Body>
                        {react.state.errors.length > 0 ? <Error errors={react.state.errors}/> : ''}

                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>User Name:</label>
                                    <input type="text" className="form-control" onChange={react.handleUserName} />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>User Email:</label>
                                    <input type="email" className="form-control" onChange={react.handleUserEmail} />
                                </div>
                            </div>
                        </div>

                        <br />
                    </Modal.Body>
                    <Modal.Footer>
                        <button className="btn btn-primary">Send Invitation</button>
                        <button type="button" className='btn btn-default' onClick={ react.props.closeModal }>Cancel</button>
                    </Modal.Footer>
                </form>
            </div>
        )
    }
}

export default CreateModal