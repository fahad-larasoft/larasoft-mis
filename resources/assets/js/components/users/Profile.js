import React, {Component} from 'react';

import {Link} from 'react-router-dom';
import BasicInfoIndex from '../basic-info/Index';
import ExperienceIndex from '../experiences/Index';
import QualificationIndex from '../qualifications/Index';
import ProjectIndex from './projects/Index';
import AppraisalIndex from '../appraisals/Index';
import axios from "axios/index";
import Success from "../common/Success";
import Modal from 'react-bootstrap-modal';
import ProfileEditModal from "../basic-info/partials/ProfileEditModal";

class Profile extends Component{
    constructor(props){
        super(props);
        this.state = {
            user: {},
            messages: [],
            showSetting: false,
            modalIsOpen: false
        };

        this.handleProfileUpdate = this.handleProfileUpdate.bind(this);
        this.handleMessageUpdate = this.handleMessageUpdate.bind(this);
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    componentDidMount() {
        let react = this;
        let url = `user/users/${react.props.match.params.id}`;

        axios.get(url)
            .then(response => {
                react.setState({
                    user: response.data.data,
                    showSetting: true
                });
            })
            .catch(error => {
                console.log(error);
            })
    }

    openModal(){
        this.setState({
            modalIsOpen: true
        })
    }

    closeModal(){
        this.setState({
            modalIsOpen: false
        })
    }

    handleProfileUpdate(user, statusUpdate){
        this.setState({
            user: user,
        })
    }

    handleMessageUpdate(message){
        let react = this;
        let messages = react.state.messages;
        messages.push(message);
        react.setState({
            messages: messages
        });

        setTimeout(function(){
            react.setState({ messages: [] });
        }, 10000);
    }

    render(){
        let react = this;
        return(
            <div>
                <div className="col-md-12">
                {react.state.messages.length > 0 ? <Success messages={react.state.messages}/> : ''}
                </div>

                <div className="col-md-3">
                    {react.state.showSetting ?
                        <BasicInfoIndex
                        user={react.state.user}
                        handleUpdate={react.handleProfileUpdate}
                        handleMessagesUpdate={react.handleMessageUpdate}
                        /> : null}
                </div>
                <div className="col-md-9">
                    <div className="nav-tabs-custom">
                        <ul className="nav nav-tabs">
                            <li className="active"><Link to="#experiences" data-toggle="tab">Experience</Link></li>
                            <li><Link to="#qualifications" data-toggle="tab">Qualification</Link></li>
                            <li><Link to="#projects" data-toggle="tab">My projects</Link></li>
                            <li><Link to="#appraisals" data-toggle="tab">Appraisals</Link></li>
                        </ul>
                        <div className="tab-content">
                            <ExperienceIndex handleMessagesUpdate={react.handleMessageUpdate} />

                            <QualificationIndex handleMessagesUpdate={react.handleMessageUpdate}/>

                            {react.state.showSetting ? <ProjectIndex  user={react.state.user} handleMessagesUpdate={react.handleMessageUpdate} handleUpdate={react.handleProfileUpdate} /> : null}

                            {react.state.showSetting ? <AppraisalIndex user={react.state.user}  handleMessagesUpdate={react.handleMessageUpdate}/> : null}
                        </div>
                    </div>
                </div>


                <Modal
                    show={react.state.modalIsOpen}
                    onHide={react.closeModal}
                    aria-labelledby="ModalHeader"
                >
                    <Modal.Header>
                        <Modal.Title id='ModalHeader'>Pay Salary</Modal.Title>
                    </Modal.Header>
                    <ProfileEditModal
                        openModal={react.openModal}
                        closeModal={react.closeModal}
                        user={react.state.user}
                        handleUpdate={react.handleProfileUpdate}
                        handleMessagesUpdate={react.handleMessageUpdate}
                    />
                </Modal>
            </div>
        )
    }
}

export default Profile