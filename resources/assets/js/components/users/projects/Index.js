import React, {Component} from 'react';
import axios from 'axios';

class Index extends Component {
    constructor(props){
        super(props);

        this.state = {
            projects: [],
        };
    }

    componentDidMount() {
        this.fetchProjects();
    }

    fetchProjects(){
        let react = this;

        let params = {
            user_id: react.props.user.id,
        };

        axios.get('user/projects', {params: params})
            .then(response => {
                react.setState({
                    projects: response.data.data,
                })
            })
            .catch(error=> {
                if (error.response) {
                   console.log(error.response);
                }
            })

    }

    render(){
        let react = this;
        return (
            <div className="tab-pane" id="projects">

                <ul className="timeline timeline-inverse">
                    { react.state.projects.map(function (project) {
                        return (
                            <li key={project.id}>
                                <i className="fa fa-envelope bg-blue"/>

                                <div className="timeline-item">
                                    <span className="time">{project.status} <i className="fa fa-clock-o"/></span>

                                    <h3 className="timeline-header"><a href={project.url} target="_blank">{ project.name }</a></h3>

                                    <div className="timeline-body">
                                        Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                                        weebly ning heekya handango imeem plugg dopplr jibjab, movity
                                        jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                                        quora plaxo ideeli hulu weebly balihoo...
                                    </div>
                                    <div className="timeline-footer">
                                        <a href={project.url} target="_blank" className="btn btn-primary btn-xs">Read more</a>
                                        <a className="btn btn-danger btn-xs">Delete</a>
                                    </div>
                                </div>
                            </li>
                        )
                    }) }
                </ul>
            </div>
        )
    }
}

export default Index