import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import CreateModal from '../staffs/partials/CreateModal';
import Modal from 'react-bootstrap-modal';

import axios from 'axios';

class TableRow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modelIsOpen: false
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    openModal() {
        this.setState({
            modalIsOpen: true
        })
    }

    closeModal() {
        this.setState({
            modalIsOpen: false
        })
    }

    handleSubmit(event) {
        event.preventDefault();
        const {history} = this.props;

        let uri = '/users/' + this.props.obj.id;
        axios.delete(uri);
        history.push('/users');
    }

    render() {
        let react = this;
        // const deleteButton = {
        //     marginLeft: 3
        // };

        return (
            <tr>
                <td>
                    {this.props.obj.name}
                </td>
                <td>
                    {this.props.obj.email}
                </td>
                <td>
                    {this.props.obj.roles[0].name}
                </td>
                <td>
                    {
                        this.props.obj.staff ? this.props.obj.staff.basic_pay : ''}


                    {/*{this.props.obj.student ? this.props.obj.student.created_at : ''}*/}
                </td>
                <td>

                    <div>
                        <button type="button" className="btn btn-info" onClick={this.openModal}> Edit</button>&nbsp;
                        <Modal
                            show={this.state.modalIsOpen}
                            onHide={this.closeModal}
                            aria-labelledby="ModalHeader"
                            backdrop="static"
                            keyboard={false}
                        >
                            <Modal.Header>
                                <Modal.Title id='ModalHeader'>Make staff {this.props.obj.name}</Modal.Title>
                            </Modal.Header>
                            <CreateModal openModal={this.openModal} closeModal={this.closeModal}
                                         handleUpdate={this.props.handleUpdate}
                                         user={this.props.obj}
                                         handleMessagesUpdate={this.props.handleMessagesUpdate}
                            />
                        </Modal>
                        {react.props.obj.staff ?
                            <Link to={`user/${react.props.obj.staff.id}/salaries`} className="btn btn-default" onClick={this.openModal}> Salaries</Link> : ''
                        }

                        <Link replace to={`/users/${this.props.obj.id}/profile`} className="btn btn-default btn-flat">Profile</Link>

                        {/*<form onSubmit={this.handleSubmit}>*/}

                            {/*<input type="submit" value="Delete" className="btn-box-tool btn-danger" style={deleteButton}/>*/}
                        {/*</form>*/}
                    </div>

                </td>
            </tr>
        );
    }
}


export default TableRow;