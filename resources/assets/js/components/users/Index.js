import React from 'react';
import TableRow from './TableRow';
import axios from 'axios'
import Success from "../common/Success";
import Modal from 'react-bootstrap-modal';
import CreateModal from "./partials/CreateModal";

class Index extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            modalIsOpen: false,
            users: [],
            messages: []
        };

        this.handleUserUpdate = this.handleUserUpdate.bind(this);
        this.handleMessagesUpdate = this.handleMessagesUpdate.bind(this);
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    componentDidMount() {
        axios.get('/admin/users')
            .then(response => {
                this.setState({users: response.data.data});
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    openModal(){
        this.setState({
            modalIsOpen: true
        })
    }

    closeModal(){
        this.setState({
            modalIsOpen: false
        })
    }

    tabRow() {
        let react = this;
        if (react.state.users instanceof Array) {
            return react.state.users.map(function (object, i) {
                return <TableRow
                    obj={object}
                    key={i}
                    handleUpdate={react.handleUserUpdate}
                    handleMessagesUpdate={react.handleMessagesUpdate}
                />;

            })
        }
    }

    handleUserUpdate(user, statusUpdate) {
        let users = this.state.users;

        if (!statusUpdate.delete) {
            for (let i = 0; i < users.length; i++) {
                if (users[i].id === user.id) {
                    if (i > -1) {
                        users.splice(i, 1);
                    }
                }
            }

            users.push(user);
        }

        this.setState({
            users: users
        })
    }

    handleMessagesUpdate(message) {
        let react = this;
        react.setState({
            messages: [message]
        });

        setTimeout(function () {
            react.setState({messages: []});
        }, 10000);
    }

    render() {
        let react = this;
        const createButton = {
            marginTop: -27
        };

        const style = {
            width10p: {
                width: '10px'
            },
            width20p: {
                width: '20px'
            },
            width30p: {
                width: '30px'
            },
            width40p: {
                width: '40px'
            }
        };

        return (
            <div>
                {this.state.messages.length > 0 ? <Success messages={this.state.messages}/> : ''}

                <div className="box">
                    <div className="box-header">Users
                        <div>
                            <button className="btn btn-default pull-right" onClick={react.openModal} style={createButton}><i
                                className="fa fa-plus"/>&nbsp;
                                User</button>
                            <Modal
                                show={react.state.modalIsOpen}
                                onHide={react.closeModal}
                                aria-labelledby="ModalHeader"
                                backdrop="static"
                                keyboard={false}
                            >
                                <Modal.Header>
                                    <Modal.Title id='ModalHeader'>Invite user to register</Modal.Title>
                                </Modal.Header>
                                <CreateModal
                                    openModal={react.openModal}
                                    closeModal={react.closeModal}
                                    handleMessagesUpdate={react.handleMessagesUpdate}
                                />
                            </Modal>
                        </div>
                    </div>
                    <div className="box-body no-padding">

                        <table className="table table-striped">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Current Salary</th>
                                <th width="200px">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.tabRow()}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

export default Index

