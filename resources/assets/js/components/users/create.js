import React, { Component } from 'react';
import Error from "../common/Error";


class Create extends Component {
    constructor(props){
        super(props);
        this.state = {userName: '', userEmail: '', errors: []};

        this.handleUserName = this.handleUserName.bind(this);
        this.handleUserEmail = this.handleUserEmail.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleUserName(e){
        this.setState({
            userName: e.target.value
        })
    }
    handleUserEmail(e){
        this.setState({
            userEmail: e.target.value
        })
    }

    handleSubmit(e){
        let react = this;
        e.preventDefault();
        const { history } = this.props;
        const user = {
            name: this.state.userName,
            email: this.state.userEmail,
        };

        axios.post('invite', user)
            .then(function (response) {
                history.push('/users');
            })

            .catch(function (error) {
                if (error.response) {
                    var validationErrors = [];
                    $.each(error.response.data, function(key, value1){
                        $.each(value1, function(key, value2){
                            validationErrors.push(value2)
                        });
                    });
                    react.setState({ errors: validationErrors });

                    setTimeout(function(){
                        react.setState({ errors: [] });
                    }, 4000);
                }
            });
    }

    render() {
        return (
            <div>
                <div className="row">
                    <div className="col-md-12">
                        <div className="panel panel-default">
                            <div className="panel-heading">Invite User
                            </div>
                            <div className="panel-body">
                                {this.state.errors.length > 0 ? <Error errors={this.state.errors}/> : ''}
                                <form onSubmit={this.handleSubmit}>
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label>User Name:</label>
                                                <input type="text" className="form-control" onChange={this.handleUserName} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label>User Email:</label>
                                                <input type="email" className="form-control" onChange={this.handleUserEmail} />
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                    <div className="form-group">
                                        <button className="btn btn-primary">Save</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default Create

