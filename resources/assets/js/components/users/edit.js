import React, {Component} from 'react';
import axios from 'axios';
// import { Link } from 'react-router';
import {browserHistory} from 'react-router';
import { HashRouter as Router, Route, Link } from 'react-router-dom';



export default class Edit extends Component {
    constructor(props) {
        super(props);
        this.state = {name: '', email: '', errors: []};
        this.handleName = this.handleName.bind(this);
        this.handleEmail = this.handleEmail.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    componentDidMount(){
        axios.get(`/users/${this.props.match.params.id}/edit`)
            .then(response => {
                this.setState({ name: response.data.data.name, email: response.data.data.email });
            })
            .catch(function (error) {
                console.log(error);
            })
    }
    handleName(e){
        this.setState({
            name: e.target.value
        })
    }
    handleEmail(e){
        this.setState({
            email: e.target.value
        })
    }


    handleSubmit(event) {
        var react = this;
        event.preventDefault();
        const { history } = this.props;
        const user = {
            name: this.state.name,
            email: this.state.email
        };



        let uri = '/admin/users/'+this.props.match.params.id;
        axios.put(uri, user)
            .then(function (response) {
                history.push('/users');
            })
            .catch(function (error) {
                if (error.response) {
                    let validationErrors = [];
                    $.each(error.response.data, function(key, value1){
                        $.each(value1, function(key, value2){
                            console.log(value2);
                            // if (this.state.errors.indexOf(value2) == -1){
                            //     this.state.errors.push(value2);
                            // }
                            validationErrors.push(value2)
                        });
                    });
                    console.log(validationErrors);
                    react.setState({ errors: validationErrors });

                    setTimeout(function(){
                        react.setState({ errors: [] });
                    }, 4000);
                }
            });
    }
    render(){
        return (

        <div>
            <div className="row">
                <div className="col-md-12">
                    <div className="panel panel-default">
                        <div className="panel-heading">Update {this.state.name}</div>
                        <div className="panel-body">
                            {this.state.errors.length > 0 ?

                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="alert alert-danger">
                                            <ul>
                                                {this.state.errors.map((error, key) => {
                                                    return <li key={key}> { error } </li>

                                                })}
                                            </ul>
                                        </div>
                                    </div>
                                </div> :
                                <span>&nbsp;</span>
                            }
                            <form onSubmit={this.handleSubmit}>
                                <div className="form-group">
                                    <label>User Name</label>
                                    <input type="text"
                                           className="form-control"
                                           value={this.state.name}
                                           onChange={this.handleName} />
                                </div>


                                <div className="form-group">
                                    <label name="product_body">User Email</label>

                                    <input type="email"
                                           className="form-control"
                                           value={this.state.email}
                                           onChange={this.handleEmail} />
                                </div>


                                <div className="form-group">
                                    <button className="btn btn-primary">Update</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>





        )
    }
}
