import React, {Component} from 'react';
import Model from 'react-bootstrap-modal';
import QualificationRow from './partials/QualificationRow';
import CreateModal from './partials/CreateModal';
import axios from 'axios';

class Index extends Component{
    constructor(props){
        super(props);
        this.state = {
            qualifications: [],
            messages: [],
            modalIsOpen: false,
        };

        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.handleToUpdate = this.handleToUpdate.bind(this);
    }

    openModal(){
        this.setState({
            modalIsOpen: true
        })
    }

    closeModal() {
        this.setState({
            modalIsOpen: false
        })
    }

    handleToUpdate(qualification, statusUpdate){
        let react = this;
        let qualifications = react.state.qualifications;

        if (statusUpdate.delete) {
            for (let i = 0; i < qualifications.length; i++) {
                if (qualifications[i].id === qualification.id) {
                    if (i > -1) {
                        react.setState({
                            qualifications: []
                        });
                        qualifications.splice(i, 1);
                    }
                }
            }
        }
        if (!statusUpdate.delete) {
            for (let i = 0; i < qualifications.length; i++) {
                if (qualifications[i].id === qualification.id) {
                    if (i > -1) {
                        qualifications.splice(i, 1);
                    }
                }
            }

            qualifications.push(qualification);
        }
        react.setState({
            qualifications: qualifications
        });
    }


    componentDidMount() {
        axios.get('user/qualifications')
            .then(response => {
                this.setState({
                    qualifications: response.data.data
                });
            })
            .catch(error => {
                console.log(error)
            })
    }

    qualificationRow(){
        let react = this;
        let qualifications = this.state.qualifications;
        if(qualifications instanceof Array){
            if (qualifications.length > 0){
                return qualifications.map(function(qualification, i){
                    return <QualificationRow
                        qualification={qualification}
                        key={i}
                        handleUpdate={react.handleToUpdate}
                        handleMessagesUpdate={react.props.handleMessagesUpdate}
                    />;
                })
            }else {
                return (
                    <div className="tab-pane" id="qualifications">
                        <div>No Qualification</div>
                    </div>
                )
            }

        }
    }

    render(){
        let react = this;
        return(
            <div className="tab-pane" id="qualifications">
                <button type="button" className="pull-right btn-default" onClick={this.openModal}><i className="fa fa-plus"/> Create</button>
                <Model
                show={react.state.modalIsOpen}
                onHide={react.closeModal}
                aria-labelledby="ModalHeader"
                backdrop="static"
                keyboard= {false}
                >
                    <CreateModal
                        openModal={react.openModal}
                        closeModal={react.closeModal}
                        handleToUpdate={react.handleToUpdate}
                        handleMessagesUpdate={react.props.handleMessagesUpdate}
                    />
                </Model>
                {react.qualificationRow()}
            </div>
        )
    }
}

export default Index