import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import EditModal from './EditModal';
import Modal from 'react-bootstrap-modal';
import ConfirmDelete from "../../common/ConfirmDelete";
import axios from 'axios';

class QualificationRow extends Component{
    constructor(props){
        super(props);
        this.state = {
            qualification: props.qualification,
            modalIsOpen: false,
            deleteModalIsOpen: false,
        };

        this.openModal = this.openModal.bind(this);
        this.openDeleteModal = this.openDeleteModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.closeDeleteModal = this.closeDeleteModal.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    openModal(){
        this.setState({
            modalIsOpen: true
        })
    }

    closeModal(){
        this.setState({
            modalIsOpen: false
        })
    }

    openDeleteModal(){
        this.setState({
            deleteModalIsOpen: true
        })
    }

    closeDeleteModal(){
        this.setState({
            deleteModalIsOpen: false
        })
    }

    handleDelete(){
        let react = this;
        let statusUpdate = {
            create: false,
            update: false,
            delete: true,
        };

        axios.delete(`user/qualifications/${react.state.qualification.id}`)
            .then(response => {
                react.props.handleUpdate(react.state.qualification, statusUpdate);
                console.log(response);
            })
            .catch(error => {
                console.log(error)
            })
    }

    render(){
        let react = this;
        return (
            <div className="post">
                <br/>
                <hr/>
                <button type="button" className="pull-right btn-danger" onClick={ react.openDeleteModal }><i className="fa fa-trash"/> Delete</button>
                <Modal
                    show={ react.state.deleteModalIsOpen }
                    onHide={react.closeDeleteModal }
                    aria-labelledby="ModalHeader"
                >
                    <Modal.Header>
                        <Modal.Title id='ModalHeader'>Delete { react.state.qualification.degree_title }</Modal.Title>
                    </Modal.Header>
                    <ConfirmDelete
                        openModal={ react.openDeleteModal }
                        closeModal={ react.closeDeleteModal }
                        title={ react.state.qualification.degree_title }
                        handleDelete={ react.handleDelete }
                    />
                </Modal>
                <button type="button" className="pull-right btn-info" onClick={ this.openModal }><i className="fa fa-edit"/> Edit</button>
                <Modal
                    show={ react.state.modalIsOpen }
                    onHide={react.closeModal }
                    aria-labelledby="ModalHeader"
                    backdrop="static"
                    keyboard= {false}
                >
                    <Modal.Header>
                        <Modal.Title id='ModalHeader'>Edit {react.state.qualification.degree_title}</Modal.Title>
                    </Modal.Header>
                    <EditModal
                        openModal={ react.openModal }
                        closeModal={ react.closeModal }
                        handleUpdateEditModel={react.props.handleUpdate}
                        qualification={react.state.qualification}
                        handleMessagesUpdate={react.props.handleMessagesUpdate}
                    />
                </Modal>
                <div className="user-block">
                    <img className="img-circle img-bordered-sm" src="/vendor/adminlte/dist/img/user1-128x128.jpg" alt="user image"/>
                    <span className="username">
                          <Link to="#">{ react.state.qualification.degree_title }</Link>
                        </span>
                    <span className="description">from { react.state.qualification.start_date } - to { react.state.qualification.passing_year ? react.state.qualification.passing_year : 'continue' }</span>
                </div>
                <p>
                    {/*{react.state.qualification.description}*/}
                </p>
                <ul className="list-inline">
                    <li><Link to="#" className="link-black text-sm"><i className="fa fa-phone margin-r-5"/> Major: {react.state.qualification.major}</Link></li>
                    <li><Link to="#" className="link-black text-sm"><i className="fa fa-address-card margin-r-5"/> Passing Year: { react.state.qualification.passing_year ? react.state.qualification.passing_year : 'continue' }</Link>
                    </li>
                    <li className="pull-right">
                        <span className="description">Institute: <strong>{ react.state.qualification.institute }</strong></span>
                    </li>
                </ul>
            </div>
        )
    }
}

export  default QualificationRow