import React, {Component} from 'react';
import {Link} from "react-router-dom";

class Header extends Component{
    render(){
        return (
            <div className="navbar-custom-menu">
                <ul className="nav navbar-nav">
                    <li className="dropdown user user-menu">
                        <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                            <img src={ window.App.auth_user.image_url } className="user-image" alt="User Image"/>
                                <span className="hidden-xs">{ window.App.auth_user.name }</span>
                        </a>
                        <ul className="dropdown-menu">
                            <li className="user-header">
                                <img src={ window.App.auth_user.image_url } className="img-circle" alt="User Image"/>
                                    <p>
                                        { window.App.auth_user.name }
                                        <small>Member since { window.App.auth_user.created_at }</small>
                                    </p>
                            </li>
                            <li className="user-footer">
                                <div className="pull-left">
                                    <Link replace to={`/users/${window.App.auth_user.id}/profile`} className="btn btn-default btn-flat">Profile</Link>
                                </div>
                                <div className="pull-right">
                                    <form action="/logout" method="post">
                                        <button type="submit" className="btn btn-default btn-flat">Sign out</button>
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        )
    }
}

export default Header