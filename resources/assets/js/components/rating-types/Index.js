import React, {Component} from 'react';
import TableRow from './partials/RatingTypeRow';
import Modal from 'react-bootstrap-modal';
import CreateModal from './partials/CreateModal';
import axios from 'axios'
import Success from "../common/Success";

class Index extends Component {
    constructor(props) {
        super(props);

        this.state = {
            ratingTypes: [],
            messages: [],
            is_admin: window.App.is_admin,
            modalIsOpen: false,
        };

        this.handleUpdate = this.handleUpdate.bind(this);
        this.handleMessageUpdate = this.handleMessageUpdate.bind(this);
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    openModal() {
        this.setState({
            modalIsOpen: true
        })
    }

    closeModal() {
        this.setState({
            modalIsOpen: false
        })
    }

    componentDidMount() {
        let react = this;
        let url = `/admin/rating-types`;

        axios.get(url)
            .then(response => {
                this.setState({
                    ratingTypes: response.data.data,
                    staff: response.data.data,
                });
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    tabRow() {
        let react = this;
        if (react.state.ratingTypes instanceof Array) {
            return react.state.ratingTypes.map(function (object, i) {
                return <TableRow
                    obj={object}
                    key={i}
                    handleUpdate={react.handleUpdate}
                    handleMessagesUpdate={react.handleMessageUpdate}
                />;

            })
        }
    }

    handleUpdate(rating_type, statusUpdate) {
        let ratingTypes = this.state.ratingTypes;
        if (statusUpdate.delete) {
            for (let i = 0; i < ratingTypes.length; i++) {
                if (ratingTypes[i].id === rating_type.id) {
                    if (i > -1) {
                        ratingTypes.splice(i, 1);
                    }
                }
            }
        }
        if (!statusUpdate.delete) {
            for (let i = 0; i < ratingTypes.length; i++) {
                if (ratingTypes[i].id === rating_type.id) {
                    if (i > -1) {
                        ratingTypes.splice(i, 1);
                    }
                }
            }

            ratingTypes.push(rating_type);
        }

        this.setState({
            ratingTypes: ratingTypes
        })
    }

    handleMessageUpdate(message) {
        let react = this;
        let messages = react.state.messages;
        messages.push(message);
        react.setState({
            messages: messages
        });

        setTimeout(function () {
            react.setState({messages: []});
        }, 10000);
    };

    render() {
        const createButton = {
            marginTop: -27
        };

        let react = this;
        return (
            <div>
                <div className="box-body no-padding">
                    {react.state.messages.length > 0 ? <Success messages={react.state.messages}/> : ''}

                    <div className="box">
                        <div className="box-header">Rating Types
                            <div>
                                <button onClick={react.openModal} className="btn btn-default pull-right"
                                        style={createButton}><i className="fa fa-plus"/>&nbsp;
                                    Create
                                </button>

                                <Modal
                                    show={react.state.modalIsOpen}
                                    onHide={react.closeModal}
                                    aria-labelledby="ModalHeader"
                                >
                                    <Modal.Header>
                                        <Modal.Title id='ModalHeader'>Create Rating type</Modal.Title>
                                    </Modal.Header>
                                    <CreateModal
                                        openModal={react.openModal}
                                        closeModal={react.closeModal}
                                        handleUpdate={react.handleUpdate}
                                        handleMessagesUpdate={react.handleMessageUpdate}
                                    />
                                </Modal>
                            </div>
                        </div>


                        <table className="table table-striped">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th width="200px">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            {react.tabRow()}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

export default Index

