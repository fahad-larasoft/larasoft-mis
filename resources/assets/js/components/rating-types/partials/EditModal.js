import React, {Component} from 'react';
import Modal from 'react-bootstrap-modal';
import moment from 'moment';
import DatePicker from 'react-datepicker';

import 'react-datepicker/dist/react-datepicker.css';
import Error from "../../common/Error";

class PayModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rating_type: {
                name: '',
            },
            errors: [],
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleBasicSalary = this.handleBasicSalary.bind(this);
    }

    componentDidMount(){
        let react = this;
        react.setState({
            rating_type: react.props.rating_type,
        })
    }

    handleBasicSalary(e){
        let rating_type = this.state.rating_type;
        rating_type.name = e.target.value;
        this.setState({
            rating_type: rating_type
        })
    }


    handleSubmit(e) {
        let react = this;
        e.preventDefault();

        let statusUpdate = {
            create: false,
            update: true,
            delete: false,
        };

        axios.put(`admin/rating-types/${react.state.rating_type.id}`, react.state.rating_type)
            .then(response => {
                react.props.handleUpdate(response.data.data, statusUpdate);
                react.props.handleMessagesUpdate(response.data.message);
                this.props.closeModal();
            })
            .catch(function (error) {
                console.log(error.response.data.errors);
                if (error.response) {
                    react.setState({ errors: error.response.data.errors });
                    setTimeout(function(){
                        react.setState({ errors: [] });
                    }, 4000);
                }
            });

    }

    render() {
        let react = this;
        return (
            <div className="modalLayout">
                <form onSubmit={ react.handleSubmit }>
                    <Modal.Body>
                        {react.state.errors.length > 0 ? <Error errors={react.state.errors}/> : ''}

                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Type Name:</label>
                                    <input type="text" value={react.state.rating_type.name || ''} onChange={ react.handleBasicSalary } className="form-control" />
                                </div>
                            </div>
                        </div>

                        <br />
                    </Modal.Body>
                    <Modal.Footer>
                        <button className="btn btn-primary">Update</button>
                        <button type="button" className='btn btn-default' onClick={ react.props.closeModal }>Cancel</button>
                    </Modal.Footer>
                </form>
            </div>
        )
    }
}

export default PayModal