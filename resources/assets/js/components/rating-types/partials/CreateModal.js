import React, {Component} from 'react';
import Modal from 'react-bootstrap-modal';

import 'react-datepicker/dist/react-datepicker.css';
import Error from "../../common/Error";

class CreateModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rating_type: {
                name: '',
            },
            errors: [],
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleName = this.handleName.bind(this);
    }

    handleName(e){
        let rating_type = this.state.rating_type;
        rating_type.name = e.target.value;
        this.setState({
            rating_type: rating_type
        })
    }


    handleSubmit(e) {
        let react = this;
        e.preventDefault();

        let statusUpdate = {
            create: true,
            update: false,
            delete: false,
        };

        axios.post('admin/rating-types', react.state.rating_type)
            .then(response => {
                react.props.handleUpdate(response.data.data, statusUpdate);
                react.props.handleMessagesUpdate(response.data.message);
                this.props.closeModal();
            })
            .catch(function (error) {
                console.log(error.response.data.errors);
                if (error.response) {
                    react.setState({ errors: error.response.data.errors });
                    setTimeout(function(){
                        react.setState({ errors: [] });
                    }, 4000);
                }
            });

    }

    render() {
        let react = this;
        return (
            <div className="modalLayout">
                <form onSubmit={ react.handleSubmit }>
                    <Modal.Body>
                        {react.state.errors.length > 0 ? <Error errors={react.state.errors}/> : ''}

                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Type Name:</label>
                                    <input type="text" value={react.state.rating_type.name || ''} onChange={ react.handleName } placeholder="Enter rating type name" className="form-control" />
                                </div>
                            </div>
                        </div>

                        <br />
                    </Modal.Body>
                    <Modal.Footer>
                        <button className="btn btn-primary">Save</button>
                        <button type="button" className='btn btn-default' onClick={ react.props.closeModal }>Cancel</button>
                    </Modal.Footer>
                </form>
            </div>
        )
    }
}

export default CreateModal