import React, {Component} from 'react';
import Modal from 'react-bootstrap-modal';

import axios from 'axios';
import EditModal from "./EditModal";
import ConfirmDelete from "../../common/ConfirmDelete";

class SalaryRow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modelIsOpen: false,
            deleteModalIsOpen: false
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.openDeleteModal = this.openDeleteModal.bind(this);
        this.closeDeleteModal = this.closeDeleteModal.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    openModal() {
        this.setState({
            modalIsOpen: true
        })
    }

    closeModal() {
        this.setState({
            modalIsOpen: false
        })
    }


    handleSubmit(event) {
        event.preventDefault();
        const {history} = this.props;

        let uri = '/users/' + this.props.obj.id;
        axios.delete(uri);
        history.push('/users');
    }

    openDeleteModal(){
        this.setState({
            deleteModalIsOpen: true
        })
    }

    closeDeleteModal(){
        this.setState({
            deleteModalIsOpen: false
        })
    }

    handleDelete(){
        let react = this;
        let statusUpdate = {
            create: false,
            update: false,
            delete: true,
        };

        axios.delete(`admin/rating-types/${this.props.obj.id}`)
            .then(response => {
                console.log(response);
                react.props.handleUpdate(react.props.obj, statusUpdate);
                react.props.handleMessagesUpdate(response.data.message);
                react.closeDeleteModal();
            })
            .catch(error => {
                console.log(error)
            })
    }

    render() {
        let react = this;

        return (
            <tr>
                <td>
                    {react.props.obj.id}
                </td>
                <td>
                    {react.props.obj.name}
                </td>
                <td>

                    <div>
                        <button type="button" className="btn btn-info" onClick={react.openModal}> Edit
                        </button>&nbsp;
                        <button type="button" className="btn btn-danger" onClick={react.openDeleteModal}> Delete
                        </button>

                        <Modal
                            show={ react.state.deleteModalIsOpen }
                            onHide={react.closeDeleteModal }
                            aria-labelledby="ModalHeader"
                        >
                            <Modal.Header>
                                <Modal.Title id='ModalHeader'>Delete {react.props.obj.name}</Modal.Title>
                            </Modal.Header>
                            <ConfirmDelete
                                openModal={ react.openDeleteModal }
                                closeModal={ react.closeDeleteModal }
                                title={ react.props.obj.name }
                                handleDelete={ react.handleDelete }
                            />
                        </Modal>

                        &nbsp;&nbsp;
                        <Modal
                            show={react.state.modalIsOpen}
                            onHide={react.closeModal}
                            aria-labelledby="ModalHeader"
                        >
                            <Modal.Header>
                                <Modal.Title id='ModalHeader'>Edit Rating type of {react.props.obj.name}</Modal.Title>
                            </Modal.Header>
                            <EditModal
                                openModal={react.openModal}
                                closeModal={react.closeModal}
                                handleUpdate={react.props.handleUpdate}
                                handleMessagesUpdate={react.props.handleMessagesUpdate}
                                rating_type={react.props.obj}
                            />
                        </Modal>
                    </div>

                </td>
            </tr>
        );
    }
}


export default SalaryRow;