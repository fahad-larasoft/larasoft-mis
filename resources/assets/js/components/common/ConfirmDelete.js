import React, {Component} from 'react';
import Modal from 'react-bootstrap-modal';

class ConfirmDelete extends Component{
    render(){
        let react = this;
        return(
            <div className="modalLayout">
                    <Modal.Body>
                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <span>Are you sure, you want to delete {react.props.title} ?</span>
                                </div>
                            </div>
                        </div>

                        <br />
                    </Modal.Body>
                    <Modal.Footer>
                        <button type="button" onClick={react.props.handleDelete} className="btn btn-danger">Delete</button>
                        <button type="button" className='btn btn-default' onClick={ react.props.closeModal }>Cancel</button>
                    </Modal.Footer>
            </div>
        )
    }
}

export default ConfirmDelete