import React, {Component} from 'react';

class Success extends Component{
    render(){
        let react  = this;
        return (
            <div className="row">
                <div className="col-md-12">
                    <div className="alert alert-success">
                        <ul>
                            {react.props.messages.map((message, key) => {
                                return <li key={key}> {message} </li>
                            })}
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}

export default Success