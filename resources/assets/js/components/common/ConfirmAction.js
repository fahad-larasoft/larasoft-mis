import React from 'react';
import Modal from 'react-bootstrap-modal';

ConfirmAction = props =>{
        return(
            <div className="modalLayout">
                    <Modal.Body>
                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <span>Are you sure, you want to {props.title} ?</span>
                                </div>
                            </div>
                        </div>

                        <br />
                    </Modal.Body>
                    <Modal.Footer>
                        <button type="button" onClick={ props.handleConfirm } className="btn btn-danger">Confirm</button>
                        <button type="button" className='btn btn-default' onClick={ props.closeModal }>Cancel</button>
                    </Modal.Footer>
            </div>
        )
};

export default ConfirmAction