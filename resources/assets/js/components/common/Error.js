import React, {Component} from 'react';

class Error extends Component{
    render(){
        let react  = this;
        return(
            <div className="row">
                <div className="col-md-12">
                    <div className="alert alert-danger">
                        <ul>
                            {react.props.errors.map((error, key) => {
                                return <li key={key}> {error} </li>
                            })}
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}

export default Error