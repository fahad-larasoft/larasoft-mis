import React, {Component} from 'react';
import moment from 'moment';
import Modal from 'react-bootstrap-modal';
import ProfileImageEditModal from "./partials/ProfileImageEditModal";
import ProfileEditModal from "./partials/ProfileEditModal";

class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalIsOpen: false,
            profileModalIsOpen: false
        };
    }

    openModal = () =>
        this.setState({
            modalIsOpen: true
        });


    closeModal = () =>
        this.setState({
            modalIsOpen: false
        });

    openProfileEditModal = () =>
        this.setState({
            profileModalIsOpen: true
        });

    closeProfileModal = () =>
        this.setState({
            profileModalIsOpen: false
        });

    render() {
        let react = this;
        return (
            <div className="box box-primary">
                <div className="box-body box-profile">

                    <img onClick={react.openModal} className="profile-user-img img-responsive img-circle"
                         src={react.props.user.image_url} alt="User profile picture"/>

                    <h3 className="profile-username text-center">{react.props.user.name}</h3>

                    <p className="text-muted text-center">{react.props.user.roles ? react.props.user.roles[0].name : ''}</p>

                    <ul onClick={react.openProfileEditModal} className="list-group list-group-unbordered">
                        <li className="list-group-item">
                            <b>First Name: </b> <a className="pull-right">{react.props.user.first_name}</a>
                        </li>
                        <li className="list-group-item">
                            <b>Last Name: </b> <a className="pull-right">{react.props.user.last_name}</a>
                        </li>
                        <li className="list-group-item">
                            <b>Email: </b> <a className="pull-right">{react.props.user.email}</a>
                        </li>
                        {react.props.user.date_of_birth ?
                            <li className="list-group-item">
                                <b>Date of birth: </b><span
                                className="pull-right">{react.props.user.date_of_birth}</span>
                            </li> : ''
                        }
                        {react.props.user.date_of_birth ?
                            <li className="list-group-item">
                                <b>Age: </b><span
                                className="pull-right">{moment().diff(react.props.user.date_of_birth, 'years')} Years old</span>
                            </li> : ''
                        }

                    </ul>

                    <Modal
                        show={react.state.modalIsOpen}
                        onHide={react.closeModal}
                        aria-labelledby="ModalHeader"
                    >
                        <Modal.Header>
                            <Modal.Title id='ModalHeader'>Change Image</Modal.Title>
                        </Modal.Header>
                        <ProfileImageEditModal
                            openModal={react.openModal}
                            closeModal={react.closeModal}
                            user={react.props.user}
                            handleUpdate={react.props.handleUpdate}
                            handleMessagesUpdate={react.props.handleMessagesUpdate}
                        />
                    </Modal>

                    <Modal
                        show={react.state.profileModalIsOpen}
                        onHide={react.closeProfileModal}
                        aria-labelledby="ModalHeader"
                    >
                        <Modal.Header>
                            <Modal.Title id='ModalHeader'>Change Image</Modal.Title>
                        </Modal.Header>
                        <ProfileEditModal
                            openModal={react.openProfileEditModal}
                            closeModal={react.closeProfileModal}
                            user={react.props.user}
                            handleUpdate={react.props.handleUpdate}
                            handleMessagesUpdate={react.props.handleMessagesUpdate}
                        />
                    </Modal>
                </div>
            </div>
        )
    }
}

export default Index