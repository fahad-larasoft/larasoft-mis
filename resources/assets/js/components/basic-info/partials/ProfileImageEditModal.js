import React, {Component} from 'react'
import axios from "axios/index";
import Modal from 'react-bootstrap-modal';
import Error from "../../common/Error";

class ProfileImageEditModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            image: '',
            imagePreviewUrl: '/vendor/adminlte/dist/img/user4-128x128.jpg',
            errors: [],
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleImageChange = this.handleImageChange.bind(this);
    }

    componentDidMount() {
        this.setState({
            imagePreviewUrl: this.props.user.image_url,
        })
    }

    handleImageChange(e) {
        e.preventDefault();

        let fileReader = new FileReader();
        let file = e.target.files[0];

        fileReader.onload = () => {
            this.setState({
                image: fileReader.result,
                imagePreviewUrl: fileReader.result
            })
        };

        fileReader.readAsDataURL(file);
    }

    handleSubmit(e) {
        e.preventDefault();
        let react = this;
        let url = `user/users/${window.App.auth_user.id}`;

        let statusUpdate = {
            create: false,
            update: true,
            delete: false,
        };

        let user = react.props.user;

        if (react.state.image !== ''){
            user.image = react.state.image;
        }

        axios.put(url, user)
            .then(response => {
                react.props.handleUpdate(response.data.data, statusUpdate);
                react.props.handleMessagesUpdate(response.data.message);
                react.props.closeModal();
            })
            .catch(function (error) {
                if (error.response) {
                    react.setState({
                        errors: error.response.data.errors,
                        password: '',
                        password_confirmation: '',
                    });
                    setTimeout(function () {
                        react.setState({errors: []});
                    }, 10000);
                }
            });
    }



    render() {
        let react = this;
        let {imagePreviewUrl} = react.state;

        return (
            <div className="modalLayout">
                <form onSubmit={react.handleSubmit}>
                    <Modal.Body>

                        {react.state.errors.length > 0 ? <Error errors={react.state.errors}/> : ''}

                        <div className="row">
                            <div className="col-md-6 col-md-offset-3">

                                {imagePreviewUrl ?
                                    <img className="img-responsive" src={imagePreviewUrl} alt="User profile picture"/> : ''
                                }
                                <input className="form-control" type="file" onChange={react.handleImageChange}/>
                            </div>
                        </div>

                    </Modal.Body>
                    <Modal.Footer>
                        <button type="submit" className="btn btn-success">Save changes</button>
                        <button type="button" className='btn btn-default' onClick={react.props.closeModal}>Cancel
                        </button>
                    </Modal.Footer>
                </form>
            </div>
        )
    }
}

export default ProfileImageEditModal