import React, {Component} from 'react'
import axios from "axios/index";
import Modal from 'react-bootstrap-modal';
import Error from "../../common/Error";
import moment from 'moment';
import DatePicker from "react-datepicker";

class ProfileEditModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {
                first_name: '',
                last_name: '',
                email: '',
                name: '',
            },
            date_of_birth: moment(),
            password: '',
            password_confirmation: '',
            errors: [],
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleFirstName = this.handleFirstName.bind(this);
        this.handleLastName = this.handleLastName.bind(this);
        this.handleEmail = this.handleEmail.bind(this);
        this.handleName = this.handleName.bind(this);
        this.handlePassword = this.handlePassword.bind(this);
        this.handlePasswordConfirmation = this.handlePasswordConfirmation.bind(this);
        this.handleDateOfBirth = this.handleDateOfBirth.bind(this);
    }

    componentDidMount() {
        this.setState({
            user: this.props.user,
            date_of_birth:this.props.user.date_of_birth ? moment(this.props.user.date_of_birth) : null,
        })
    }


    handleDateOfBirth(date) {
        this.setState({
            date_of_birth: date
        })
    }

    handleSubmit(e) {
        e.preventDefault();
        let react = this;
        let url = `user/users/${window.App.auth_user.id}`;

        let statusUpdate = {
            create: false,
            update: true,
            delete: false,
        };

        let user = react.state.user;
        if (react.state.password !== '' || react.state.password_confirmation !== '') {
            user.password = react.state.password;
            user.password_confirmation = react.state.password_confirmation;
        }


        user.date_of_birth = react.state.date_of_birth ? react.state.date_of_birth.format('YYYY-MM-DD') : null;

        axios.put(url, user)
            .then(response => {
                react.props.handleUpdate(response.data.data, statusUpdate);
                react.props.handleMessagesUpdate(response.data.message);
                react.props.closeModal();
            })
            .catch(function (error) {
                if (error.response) {
                    react.setState({
                        errors: error.response.data.errors,
                        password: '',
                        password_confirmation: '',
                    });
                    setTimeout(function () {
                        react.setState({errors: []});
                    }, 10000);
                }
            });
    }

    handleFirstName(e) {
        let user = this.state.user;
        user.first_name = e.target.value;
        this.setState({
            user: user,
        })
    }

    handleLastName(e) {
        let user = this.state.user;
        user.last_name = e.target.value;
        this.setState({
            user: user,
        })
    }

    handleEmail(e) {
        let user = this.state.user;
        user.email = e.target.value;
        this.setState({
            user: user,
        })
    }

    handleName(e) {
        let user = this.state.user;
        user.name = e.target.value;
        this.setState({
            user: user,
        })
    }

    handlePassword(e) {
        this.setState({
            password: e.target.value,
        })
    }

    handlePasswordConfirmation(e) {
        this.setState({
            password_confirmation: e.target.value,
        })
    }


    render() {
        let react = this;
        let {imagePreviewUrl} = react.state;
        let $imagePreview = null;

        return (
            <div className="modalLayout">
                <form onSubmit={react.handleSubmit}>
                    <Modal.Body>

                        {react.state.errors.length > 0 ? <Error errors={react.state.errors}/> : ''}

                        <div className="row">
                            <div className="col-md-12">
                                <label>First Name</label>
                                <input type="text" value={react.state.user.first_name || ''}
                                       onChange={react.handleFirstName} className="form-control"
                                       placeholder="First Name"/>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <label>Last Name</label>
                                <input type="text" value={react.state.user.last_name || ''}
                                       onChange={react.handleLastName} className="form-control"
                                       placeholder="First Name"/>
                            </div>
                        </div>


                        <div className="row">
                            <div className="col-md-12">
                                <label>User Name</label>
                                <input type="text" value={react.state.user.name || ''} onChange={react.handleName}
                                       className="form-control" placeholder="User Name"/>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <label>Email</label>
                                <input type="email" value={react.state.user.email || ''}
                                       onChange={react.handleEmail}
                                       className="form-control" placeholder="Email"/>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Date of birth:</label>
                                    <DatePicker selected={react.state.date_of_birth}
                                                className="form-control"
                                                onChange={react.handleDateOfBirth}/>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <label>Password</label>
                                <input type="password" value={react.state.password} onChange={react.handlePassword}
                                       className="form-control" placeholder="Password"/>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <label>Confirm Password</label>
                                <input type="password" value={react.state.password_confirmation}
                                       onChange={react.handlePasswordConfirmation} className="form-control"
                                       placeholder="Confirm Password"/>
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <button type="submit" className="btn btn-success">Save changes</button>
                        <button type="button" className='btn btn-default' onClick={react.props.closeModal}>Cancel
                        </button>
                    </Modal.Footer>
                </form>
            </div>
        )
    }
}

export default ProfileEditModal