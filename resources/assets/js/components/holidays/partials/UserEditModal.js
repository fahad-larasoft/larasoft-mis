import React, {Component} from 'react';
import Modal from 'react-bootstrap-modal';
import moment from 'moment';
import DatePicker from 'react-datepicker';
import axios from 'axios';
import 'react-datepicker/dist/react-datepicker.css';
import Error from "../../common/Error";

class UserEditModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            holiday: {
                title: '',
                days: 1,
            },
            start: moment(),
            errors: [],
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleStart = this.handleStart.bind(this);
        this.handleDays = this.handleDays.bind(this);
        this.handleTitle = this.handleTitle.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    componentDidMount(){
        let react = this;
        let holiday= react.props.holiday;
        holiday.days = holiday.end.diff(holiday.start, 'days');
        this.setState({
            holiday: holiday,
            start: holiday.start,
        })
    }

    handleStart(date){
        this.setState({
            start: date
        })
    }

    handleDays(e){
        let holiday = this.state.holiday;
        holiday.days = e.target.value;
        this.setState({
            holiday: holiday
        })
    }

    handleTitle(e){
        let holiday = this.state.holiday;
        holiday.title = e.target.value;
        this.setState({
            holiday: holiday
        })
    }

    handleSubmit(e) {
        let react = this;
        e.preventDefault();

        let statusUpdate = {
            create: false,
            update: true,
            delete: false,
        };

        if (react.state.holiday.days > 0 && react.state.holiday.days <= 25) {
            let selected_start_date = moment(react.state.start.format('Y-MM-DD'));

            let selected_end_date = selected_start_date.add(react.state.holiday.days, 'days');

            let isCalendarEventOverlapping = react.isCalendarEventOverlapping(react.state.start, selected_end_date, react.props.calendar_events);

            let holiday = {
                title: react.state.holiday.title
            };
            holiday.start = react.state.start.format('YYYY-MM-DD');
            holiday.end = selected_end_date.format('YYYY-MM-DD');


            if (isCalendarEventOverlapping) {
                react.setState({errors: ['Your Holiday is overlapping!!! You cannot apply for this holiday.']});
                setTimeout(function () {
                    react.setState({errors: []});
                }, 10000);
            } else {

                axios.put(`user/holidays/${react.state.holiday.id}`, holiday)
                    .then(response => {
                        react.props.handleUpdate(response.data.data, statusUpdate);
                        react.props.handleMessagesUpdate(response.data.message);
                        this.props.closeModal();
                    })
                    .catch(function (error) {
                        if (error.response) {
                            react.setState({errors: error.response.data.errors});
                            setTimeout(function () {
                                react.setState({errors: []});
                            }, 10000);
                        }
                    });

            }
        }else {
            react.setState({errors: ['limit crossed!!! please select from 1 to 25']});
            setTimeout(function () {
                react.setState({errors: []});
            }, 10000);
        }
    }

    isCalendarEventOverlapping(selected_start_date, selected_end_date, calendar_events){
        let react = this;
        for (let i in calendar_events)
        {
            if (calendar_events[i].start.isSame(selected_start_date)){
            }else {
                if (calendar_events[i].id !== selected_start_date.id)
                {
                    if (selected_start_date.isBefore(calendar_events[i].end) && selected_end_date.isAfter(calendar_events[i].start))
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    handleDelete() {
        let react = this;

        let statusUpdate = {
            create: false,
            update: false,
            delete: true,
        };

        axios.delete(`user/holidays/${react.state.holiday.id}`)
            .then(response => {
                react.props.handleUpdate(response.data.data, statusUpdate);
                react.props.handleMessagesUpdate(response.data.message);
                this.props.closeModal();
            })
            .catch(function (error) {
                if (error.response) {
                    react.setState({errors: error.response.data.errors});
                    setTimeout(function () {
                        react.setState({errors: []});
                    }, 10000);
                }
            });
    }

    render() {
        let react = this;
        return (
            <div className="modalLayout">
                <form onSubmit={ this.handleSubmit }>
                    <Modal.Body>
                        {react.state.errors.length > 0 ? <Error errors={react.state.errors}/> : ''}

                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Title :</label>
                                    <input type="text" value={react.state.holiday.title} onChange={ react.handleTitle } className="form-control" />
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Start Date:</label>
                                    <DatePicker  selected={ react.state.start }
                                                 className={'form-control'}
                                                 onChange={ react.handleStart }/>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>No of Days :</label>
                                    <input type="text" value={react.state.holiday.days} onChange={ react.handleDays } className="form-control" />
                                </div>
                            </div>
                        </div>

                        <br />
                    </Modal.Body>
                    <Modal.Footer>
                        <button type="button" className='btn btn-danger' onClick={react.handleDelete}>Delete</button>
                        <button className="btn btn-primary">Update</button>
                        <button type="button" className='btn btn-default' onClick={ react.props.closeModal }>Cancel</button>
                    </Modal.Footer>
                </form>
            </div>
        )
    }
}

export default UserEditModal