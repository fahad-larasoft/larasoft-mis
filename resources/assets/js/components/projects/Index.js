import React, {Component} from 'react';
import ProjectRow from './partials/ProjectRow';
import Modal from 'react-bootstrap-modal';
import CreateModal from './partials/CreateModal';
import axios from 'axios'
import Success from "../common/Success";

class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            projects: [],
            staffs: [],
            messages: [],
            is_admin: true,
            staff_id: 1,
            modalIsOpen: false,
        };

        this.handleProjectUpdate = this.handleProjectUpdate.bind(this);
        this.handleMessageUpdate = this.handleMessageUpdate.bind(this);
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    openModal(){
        this.setState({
            modalIsOpen: true
        })
    }

    closeModal(){
        this.setState({
            modalIsOpen: false
        })
    }

    componentDidMount() {
        this.fetchProjects();
        this.fetchStaffs();
    }

    fetchProjects(){
        let react = this;
        let url = `/admin/projects`;

        let params = {
            id: react.state.staff_id
        };

        axios.get(url, {
            params: params
        })
            .then(response => {
                react.setState({projects: response.data.data});
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    fetchStaffs(){
        let react = this;
        let params = {
            column: 'name'
        };
        axios.get(`/admin/users`, {
            params: params,
        })
            .then(response => {
                react.setState({staffs: response.data.data});
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    tabRow() {
        let react = this;
        if (react.state.projects instanceof Array) {
            return react.state.projects.map(function (object, i) {
                return <ProjectRow
                    obj={object}
                    key={i}
                    handleUpdate={react.handleProjectUpdate}
                    handleMessagesUpdate={react.handleMessageUpdate}
                    is_admin={react.state.is_admin}
                    staffs={react.state.staffs}
                />;

            })
        }
    }

    handleProjectUpdate(project, statusUpdate){
        let projects = this.state.projects;
        if (!statusUpdate.delete) {
            for (let i = 0; i < projects.length; i++) {
                if (projects[i].id === project.id) {
                    if (i > -1) {
                        projects.splice(i, 1);
                    }
                }
            }

            projects.push(project);
        }

        this.setState({
            projects: projects
        })
    }

    handleMessageUpdate(message){
        let react = this;
        let messages = react.state.messages;
        messages.push(message);
        react.setState({
            messages: messages
        });

        setTimeout(function(){
            react.setState({ messages: [] });
        }, 10000);
    }

    render() {
        const createButton = {
            marginTop: -27
        };

        const style = {
            width10p: {
                width: '10px'
            },
            width20p: {
                width: '20px'
            },
            width30p: {
                width: '30px'
            },
            width40p: {
                width: '40px'
            }
        };
        let react = this;
        return (
            <div>
                <div className="box-body no-padding">
                    {react.state.messages.length > 0 ?
                            <Success messages={react.state.messages}/> : ''
                    }

                    <div className="box">
                    <div className="box-header">Projects
                        <div>
                            {react.state.is_admin ?
                                <button onClick={react.openModal} className="btn btn-default pull-right" style={createButton}><i className="fa fa-plus"/>&nbsp;
                                    Create</button> : ''
                            }

                            <Modal
                                show={react.state.modalIsOpen}
                                onHide={react.closeModal}
                                aria-labelledby="ModalHeader"
                                backdrop="static"
                                keyboard={false}
                            >
                                <Modal.Header>
                                    <Modal.Title id='ModalHeader'>Create Project</Modal.Title>
                                </Modal.Header>
                                <CreateModal openModal={react.openModal} closeModal={react.closeModal}
                                             handleUpdate={react.handleProjectUpdate}
                                             staff_id={react.state.staff_id}
                                             handleMessagesUpdate={react.handleMessageUpdate}
                                             staffs={react.state.staffs}
                                />
                            </Modal>
                        </div>
                    </div>


                        <table className="table table-striped">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Url</th>
                                <th>Assigned Date</th>
                                <th>Status</th>
                                <th>Assigned To</th>
                                <th width="200px">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            {react.tabRow()}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

export default Index

