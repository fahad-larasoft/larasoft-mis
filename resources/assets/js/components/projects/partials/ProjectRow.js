import React, {Component} from 'react';
import Modal from 'react-bootstrap-modal';

import axios from 'axios';
import EditModal from "./EditModal";

class ProjectRow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modelIsOpen: false,
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    openModal() {
        this.setState({
            modalIsOpen: true
        })
    }

    closeModal() {
        this.setState({
            modalIsOpen: false
        })
    }

    handleSubmit(event) {
        event.preventDefault();
        const {history} = this.props;

        let uri = 'admin/projects' + this.props.obj.id;
        axios.delete(uri);
        history.push('/projects');
    }

    render() {
        let react = this;
        const deleteButton = {
            marginLeft: 3
        };

        return (
            <tr>
                <td>
                    {react.props.obj.name}
                </td>
                <td>
                    <a href={react.props.obj.url} target="_blank">Go to project &nbsp;<i className="fa fa-external-link"/></a>
                </td>
                <td>
                    {react.props.obj.assigned_date}
                </td>
                <td>
                    {react.props.obj.status.toUpperCase()}
                </td>
                <td>
                    <ul>
                        {react.props.obj.staffs ? react.props.obj.staffs.map(function (obj, key) {
                            return <li key={key}>{obj.user ? obj.user.name : ''}</li>
                        }) : ''}
                    </ul>
                </td>
                <td>

                    <div>
                        {react.props.is_admin ?
                            <button type="button" className="btn btn-info" onClick={react.openModal}> Edit
                            </button> : ''
                        }
                        &nbsp;&nbsp;
                        <Modal
                            show={react.state.modalIsOpen}
                            onHide={react.closeModal}
                            aria-labelledby="ModalHeader"
                        >
                            <Modal.Header>
                                <Modal.Title id='ModalHeader'>Edit {react.props.obj.name} Project</Modal.Title>
                            </Modal.Header>
                            <EditModal openModal={react.openModal} closeModal={react.closeModal}
                                       handleUpdate={react.props.handleUpdate}
                                       project={react.props.obj}
                                       handleMessagesUpdate={react.props.handleMessagesUpdate}
                                       staffs={react.props.staffs}
                            />
                        </Modal>
                    </div>

                </td>
            </tr>
        );
    }
}


export default ProjectRow;