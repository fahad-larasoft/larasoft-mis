import React, {Component} from 'react';
import Modal from 'react-bootstrap-modal';
import moment from 'moment';
import DatePicker from 'react-datepicker';
import Select from 'react-select';

import 'react-datepicker/dist/react-datepicker.css';
import 'react-select/dist/react-select.css';
import Error from "../../common/Error";

class CreateModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            project: {
                name: '',
                url: '',
                status: 'pending',
                staffs: [],
            },
            staffs: [],
            // assigned_date: moment(),
            errors: [],
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleName = this.handleName.bind(this);
        this.handleStatus = this.handleStatus.bind(this);
        this.handleUrl = this.handleUrl.bind(this);
        this.handleAssignedDate = this.handleAssignedDate.bind(this);
        this.handleStaff = this.handleStaff.bind(this);
    }

    componentDidMount(){
        let react = this;
        let staffs = [];
        react.props.staffs.map(function (obj) {
            let staff = {};
            staff.label = obj.name;
            staff.value = obj.staff.id;
            staffs.push(staff);
        });

        react.setState({
            staffs: staffs
        });
    }

    handleName(e){
        let project = this.state.project;
        project.name = e.target.value;
        this.setState({
            project: project
        });
    }

    handleUrl(e){
        let project = this.state.project;
        project.url = e.target.value;
        this.setState({
            project: project
        })
    }

    handleStatus(e){
        let project = this.state.project;
        project.status = e.target.value;
        this.setState({
            project: project
        })
    }

    handleAssignedDate(date){
        this.setState({
            assigned_date: date
        })
    }

    handleStaff(staffs){
        let project = this.state.project;
        project.staffs = staffs;
        this.setState({
            project: project
        });
        console.log(this.state.project.staffs);
    }

    handleSubmit(e) {
        let react = this;
        e.preventDefault();

        let statusUpdate = {
            create: false,
            update: true,
            delete: false,
        };

        let project = react.state.project;
        project.staff_id = react.props.staff_id;
        // project.assigned_date = react.state.assigned_date ? react.state.assigned_date.format('YYYY-MM-DD') : null;

        axios.post('admin/projects', project)
            .then(response => {
                react.props.handleUpdate(response.data.data, statusUpdate);
                react.props.handleMessagesUpdate(response.data.message);
                this.props.closeModal();
            })
            .catch(function (error) {
                if (error.response) {
                    react.setState({ errors: error.response.data.errors });
                    setTimeout(function(){
                        react.setState({ errors: [] });
                    }, 4000);
                }
            });

    }

    render() {
        let react = this;
        return (
            <div className="modalLayout">
                <form onSubmit={ this.handleSubmit }>
                    <Modal.Body>
                        {react.state.errors.length > 0 ? <Error errors={react.state.errors}/> : ''}

                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Name :</label>
                                    <input type="text" value={react.state.project.name} onChange={ react.handleName } className="form-control" />
                                </div>
                            </div>
                        </div>

                        {/*<div className="row">*/}
                            {/*<div className="col-md-12">*/}
                                {/*<div className="form-group">*/}
                                    {/*<label>Assigning Date :</label>*/}
                                    {/*<DatePicker  selected={ react.state.assigned_date }*/}
                                                 {/*onChange={ react.handleAssignedDate }/>*/}
                                {/*</div>*/}
                            {/*</div>*/}
                        {/*</div>*/}

                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Project Url :</label>
                                    <input value={react.state.project.url} onChange={ react.handleUrl } className="form-control" />
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Status :</label>
                                    <select className="form-control" value={react.state.project.status} onChange={react.handleStatus}>
                                        <option value="pending">Pending</option>
                                        <option value="wip">WIP</option>
                                        <option value="completed">Complete</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Assign to:</label>
                                    <Select
                                        name="form-field-name"
                                        value={react.state.project.staffs}
                                        multi={true}
                                        options={react.state.staffs}
                                        onChange={this.handleStaff}
                                    />
                                </div>
                            </div>
                        </div>
                        <br />
                    </Modal.Body>
                    <Modal.Footer>
                        <button className="btn btn-primary">Save</button>
                        <button type="button" className='btn btn-default' onClick={ react.props.closeModal }>Cancel</button>
                    </Modal.Footer>
                </form>
            </div>
        )
    }
}

export default CreateModal