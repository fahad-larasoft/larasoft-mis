import React, {Component} from 'react';
import {Link} from 'react-router-dom';

class Sidebar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user: window.App.auth_user,
        };
    }

    render() {
        let react = this;
        let roles = react.state.user.roles.filter(el => {
            return el.name === 'admin';
        });
        const sidebarHeaderStyle = {
            color: '#D2D4D3',
            fontSize: 18
        };
        const sidebarMenuStyle = {
            marginLeft: 21,
            marginTop: 17
        };
        return (
            <div>
                <li style={sidebarHeaderStyle} className="header">Main Navigation</li>
                <li style={sidebarMenuStyle}><Link replace to="/"><span> <i className="fa fa-dashboard"/> Dashboard</span></Link></li>
                {/*<li style={sidebarMenuStyle}><Link to="/subjects"><span> <i className="fa fa-align-justify"/> Subjects</span></Link></li>*/}
                {window.App.is_admin ?
                    <li style={sidebarMenuStyle}><Link replace to="/users"><span> <i className="fa fa-users"/> Users</span></Link></li>:
                    ''}
                {window.App.is_admin ?
                    <li style={sidebarMenuStyle}><Link replace to="/projects"><span> <i className="fa fa-address-card"/> Projects</span></Link></li>:
                    ''}

                {window.App.is_admin ?
                <li style={sidebarMenuStyle}><Link replace to="/salaries"><span> <i className="fa fa-lock"/> Salaries</span></Link></li>:
                    ''}

                {window.App.is_staff ?
                    <li style={sidebarMenuStyle}><Link replace to={`/user/${window.App.auth_user.staff.id}/salaries`}><span> <i className="fa fa-lock"/> Salaries</span></Link></li>:
                    ''}
                {window.App.is_admin ?
                    <li style={sidebarMenuStyle}><Link replace to="/rating-types"><span> <i className="fa fa-star"/> Rating Types</span></Link></li>:
                    ''}
                <li style={sidebarMenuStyle}><Link replace to={`/appraisals`}><span> <i className="fa fa-check"/> Appraisals</span></Link></li>
                <li style={sidebarMenuStyle}><Link replace to={`/attendances`}><span> <i className="fa fa-check"/> Attendance</span></Link></li>

            </div>



        )
    }
}

export default Sidebar;