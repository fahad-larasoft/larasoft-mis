import React, { Component } from 'react';

export default class Dashboard extends Component {
    constructor(props){
        super(props);
        this.state = {password: '', password_confirmation: '', messages: [], errors: []};

        this.handleNewPassword = this.handleNewPassword.bind(this);
        this.handleConfirmPassword = this.handleConfirmPassword.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleNewPassword(e){
        this.setState({password: e.target.value})
    }
    handleConfirmPassword(e){
        this.setState({password_confirmation: e.target.value})
    }

    handleSubmit(e){
        var react = this;
        e.preventDefault();
        const { history } = this.props;
        const todo = {
            password: this.state.password,
            password_confirmation: this.state.password_confirmation
        };


        axios.post('/update-password', todo)
            .then(function (response) {

                var successMessages = [];
                successMessages.push('Password Updated successfully');
                react.setState({ messages: successMessages });
                react.setState({ password: '' });
                react.setState({ password_confirmation: '' });
                setTimeout(function(){
                    react.setState({ messages: [] });
                }, 4000);
            })
            .catch(function (error) {
                if (error.response) {
                    var validationErrors = [];
                    $.each(error.response.data.errors, function(property, errors){
                        $.each(errors, function(key, error){
                            validationErrors.push(error)
                        });
                    });
                    // console.log(validationErrors);
                    react.setState({ errors: validationErrors });

                    setTimeout(function(){
                        react.setState({ errors: [] });
                    }, 4000);
                }
            });
    }
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-8 col-md-offset-2">
                        <div className="panel panel-default">
                            <div className="panel-heading">Change Password</div>
                            <div className="panel-body">
                                {this.state.errors.length > 0 ?
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="alert alert-danger">
                                                <ul>
                                                    {this.state.errors.map((error, key) => {
                                                        return <li key={key}> { error } </li>

                                                    })}
                                                </ul>
                                            </div>
                                        </div>
                                    </div> :
                                    <span>&nbsp;</span>
                                }
                                {this.state.messages.length > 0 ?
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="alert alert-success">
                                                <ul>
                                                    {this.state.messages.map((message, key) => {
                                                        return <li key={key}> { message } </li>

                                                    })}
                                                </ul>
                                            </div>
                                        </div>
                                    </div> :
                                    <span>&nbsp;</span>
                                }
                                <form onSubmit={this.handleSubmit}>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label>New Password:</label>
                                                <input type="password" className="form-control" value={this.state.password} onChange={this.handleNewPassword} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label>Confirm Password</label>
                                                <input type="password" className="form-control" value={this.state.password_confirmation} onChange={this.handleConfirmPassword} />
                                            </div>
                                        </div>
                                    </div><br />
                                    <div className="form-group">
                                        <button className="btn btn-primary">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

