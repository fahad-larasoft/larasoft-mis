import React, {Component} from 'react';
import Modal from 'react-bootstrap-modal';
import moment from 'moment';
import DatePicker from 'react-datepicker';

import 'react-datepicker/dist/react-datepicker.css';
import Error from "../../common/Error";

class CreateModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            staff: {
                basic_pay: '',
            },
            joining_date: moment(),
            errors: [],
            messages: [],
            open: false,
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleBasicPay = this.handleBasicPay.bind(this);
        this.handleJoiningDate = this.handleJoiningDate.bind(this);
    }

    componentDidMount(){
        let react = this;
        let user = react.props.user;
        if (user.staff){
            if (user.staff.basic_pay !== null){
                console.log(user.staff);
                let staff = this.state.staff;
                staff.basic_pay = user.staff.basic_pay;

                react.setState({
                    staff: staff,
                    joining_date: moment(staff.joining_date)
                })
            }
        }
    }

    handleBasicPay(e){
        let staff = this.state.staff;
        staff.basic_pay = e.target.value;
        this.setState({
            staff: staff
        })
    }

    handleJoiningDate(date){
        this.setState({
            joining_date: date
        })
    }

    handleSubmit(e) {
        let react = this;
        e.preventDefault();

        let statusUpdate = {
            create: false,
            update: true,
            delete: false,
        };

        let staff = react.state.staff;
        staff.joining_date =react.state.joining_date ? react.state.joining_date.format('YYYY-MM-DD') : null;

        axios.put(`admin/staffs/${react.props.user.staff.id}`, staff)
            .then(response => {
                react.props.handleUpdate(response.data.data, statusUpdate);
                react.props.handleMessagesUpdate(response.data.message);
                this.props.closeModal();
            })
            .catch(error=> {
                if (error.response) {
                    react.setState({ errors: error.response.data.errors });
                    setTimeout(function(){
                        react.setState({ errors: [] });
                    }, 10000);
                }
            })
    }

    render() {
        return (
            <div className="modalLayout">
                <form onSubmit={ this.handleSubmit }>
                    <Modal.Body>
                        {this.state.errors.length > 0 ? <Error errors={this.state.errors}/> : ''}

                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Basic Pay:</label>
                                    <input type="text" value={this.state.staff.basic_pay} onChange={ this.handleBasicPay } className="form-control" />
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Joining Date:</label>
                                    <DatePicker  selected={ this.state.joining_date }
                                                 onChange={ this.handleJoiningDate }/>
                                </div>
                            </div>
                        </div>

                        <br />
                    </Modal.Body>
                    <Modal.Footer>
                        <button className="btn btn-primary">Save</button>
                        <button type="button" className='btn btn-default' onClick={ this.props.closeModal }>Cancel</button>
                    </Modal.Footer>
                </form>
            </div>
        )
    }
}

export default CreateModal