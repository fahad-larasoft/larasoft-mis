<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatingUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rating_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('appraisal_id')->unsigned();
            $table->foreign('appraisal_id')->references('id')->on('appraisals')->onDelete('cascade');
            $table->integer('rating_type_id')->unsigned();
            $table->foreign('rating_type_id')->references('id')->on('rating_types')->onDelete('cascade');
            $table->decimal('points', '2', '2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rating_user');
    }
}
