<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterHolidaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('holidays', function($table) {
            $table->string('reason')->nullable();
            $table->boolean('is_absent')->default(false);
            $table->enum('type', ['public', 'unauthorized', 'authorized', 'sickness'])->default('public');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('holidays', function($table) {
            $table->dropColumn('reason');
            $table->dropColumn('is_absent');
            $table->dropColumn('type');
        });
    }
}
