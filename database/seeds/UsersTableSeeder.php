<?php

use Illuminate\Database\Seeder;
use \App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Admin Role User
        $found = User::whereEmail('hello@larasoft.io')->first();
        if (!$found) {
            $data = [
                'name' => 'Admin',
                'first_name' => 'Admin',
                'last_name' => 'Admin',
                'email' => 'hello@larasoft.io',
                'password' => bcrypt(123456),
            ];

            $user = User::create($data);
            $user->assignRole('admin');
        }

        $found2 = User::whereEmail('user@larasoft.io')->first();
        if (!$found2) {
            $data = [
                'name' => 'Testing user',
                'first_name' => 'User',
                'last_name' => 'Ali',
                'email' => 'user@larasoft.io',
                'password' => bcrypt(123456),
            ];
        }

        $user = User::create($data);
        $user->assignRole('staff');
        $user->staff()->create();
    }


}
