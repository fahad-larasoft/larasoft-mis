<?php

use App\Models\RatingType;
use Illuminate\Database\Seeder;

class RatingTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ratingTypes = [
            ['name' => 'code quality'],
            ['name' => 'code style'],
            ['name' => 'code comments'],
            ['name' => 'personal development'],
            ['name' => 'communication with team'],
            ['name' => 'communication with client'],
            ['name' => 'attendance'],
        ];

        RatingType::insert($ratingTypes);
    }
}
