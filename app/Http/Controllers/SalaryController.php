<?php

namespace App\Http\Controllers;

use App\Facades\JsonResponse;
use App\Models\Staff;
use App\Models\User;
use App\Models\Salary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SalaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $salaries = Salary::with('staff.user')->get();

        if ($salaries){
            return JsonResponse::success('', $salaries);
        }

        return JsonResponse::error('No Salary found', $salaries);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), Salary::$rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()->all()], 422);
        }

        info($request->all());
        $staff = Staff::find($request->staff_id);

        $salary = $staff->salaries()->create($request->all());

        if ($salary){
            return JsonResponse::success('Salary created successfully', $salary->load('staff.user'));
        }

        return JsonResponse::error('Salary not created');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Salary  $salary
     * @return \Illuminate\Http\Response
     */
    public function edit(Salary $salary)
    {
        if ($salary){
            return JsonResponse::success('Salary found', $salary);
        }

        return JsonResponse::error('Salary not found');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Salary  $salary
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Salary $salary)
    {
        $validator = Validator::make($request->all(), Salary::$rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()->all()], 422);
        }

        $inputData = $request->all();

        $check = $salary->update($inputData);

        if ($check){
            return JsonResponse::success('Salary updated successfully', $salary->load('staff.user'));
        }

        return JsonResponse::error('Salary not updated');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Salary  $salary
     * @return \Illuminate\Http\Response
     */
    public function pay(Request $request, Salary $salary)
    {
        $inputData = $request->all();

        $check = $salary->update($inputData);

        if ($check){
            return JsonResponse::success('Salary paid successfully', $salary);
        }

        return JsonResponse::error('Salary not paid');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Salary $salary
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Salary $salary)
    {
        $check = $salary->delete();

        if ($check){
            return JsonResponse::success('Salary updated', $salary);
        }

        return JsonResponse::error('Salary not updated');
    }
}
