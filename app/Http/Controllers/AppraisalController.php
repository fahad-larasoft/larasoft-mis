<?php

namespace App\Http\Controllers;

use App\Facades\JsonResponse;
use App\Models\RatingUser;
use App\Models\User;
use Illuminate\Http\Request;

class AppraisalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        info($request->all());
        $user = User::find($request->id);
        $appraisals = $user->appraisals()->with(['ratingType'])->get();

        if ($appraisals){
            return JsonResponse::success('', $appraisals);
        }

        return JsonResponse::error('No record found');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RatingUser  $ratingUser
     * @return \Illuminate\Http\Response
     */
    public function show(RatingUser $ratingUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RatingUser  $ratingUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RatingUser $ratingUser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RatingUser  $ratingUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(RatingUser $ratingUser)
    {
        //
    }
}
