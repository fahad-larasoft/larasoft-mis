<?php

namespace App\Http\Controllers;

use App\Facades\JsonResponse;
use App\Models\RatingType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RatingTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rating_types = RatingType::all();

        if ($rating_types) {
            return JsonResponse::success('', $rating_types);
        }

        return JsonResponse::error('Record not found');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputData = $request->all();
        $customRules = [
            'name' => ['required','string','max:255','unique:rating_types'],
        ];
        $validator = Validator::make($inputData, $customRules);

        if ($validator->fails()) {
            return JsonResponse::error('Project not created', $validator->messages()->all(), 422);
        }

        $rating_type = RatingType::create($inputData);

        if ($rating_type){
            return JsonResponse::success('Rating type created successfully', $rating_type);
        }

        return JsonResponse::error('Rating type not created');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RatingType  $ratingType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RatingType $ratingType)
    {
        $inputData = $request->all();
        $customRules = [
            'name' => ['required', 'string', 'max:255', 'unique:rating_types,name,'. $ratingType->id],
        ];
        $validator = Validator::make($inputData, $customRules);

        if ($validator->fails()) {
            return JsonResponse::error('Project not created', $validator->messages()->all(), 422);
        }

        $rating_type = $ratingType->update($inputData);

        if ($rating_type) {
            return JsonResponse::success('Rating type created successfully', $rating_type);
        }

        return JsonResponse::error('Rating type not created');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RatingType $ratingType
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(RatingType $ratingType)
    {
        $check = $ratingType->delete();

        if ($check) {
            return JsonResponse::success('Rating type deleted successfully', $check);
        }

        return JsonResponse::error('Rating type not deleted');
    }
}
