<?php

namespace App\Http\Controllers;

use App\Facades\JsonResponse;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Junaidnasir\Larainvite\Facades\Invite;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $users = User::role('staff')->get();

        return JsonResponse::success('', $users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), User::$rules, User::$messages);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()->all()], 422);
        }
        $data = $request->all();
        $data['password'] = bcrypt($request->password);
        $user = User::create($data);

        $user->staff()->create();
        $user->assignRole('staff');

        return JsonResponse::success('User successfully added');
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        if ($user){
            return JsonResponse::success('', $user);
        }
        return JsonResponse::error('User not found');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return JsonResponse::success('', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), User::$rules, User::$messages);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()->all()], 422);
        }

        $user = User::find($id);
        $data = $request->all();
        $data['password'] = bcrypt($request->password);
        $user->update($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return JsonResponse::success('User Deleted Successfully.');
    }


    public function updatePassword(Request $request)
    {
        $this->validate($request, ['password' => 'required|min:6|confirmed']);

        auth()->user()->update(['password' => bcrypt($request->password)]);
        return response()->json(['success' => 1]);


    }

    public function invite(Request $request)
    {
        $validator = Validator::make($request->all(), ['name' => 'required|string|max:255', 'email' => 'required|email']);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()->all()], 422);
        }

        $refCode = Invite::invite($request->email , auth()->id());

        return JsonResponse::success('Invite email sent Successfully.');
    }
}
