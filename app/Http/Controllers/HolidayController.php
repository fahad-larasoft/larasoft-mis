<?php

namespace App\Http\Controllers;

use App\Facades\JsonResponse;
use App\Models\Holiday;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HolidayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $holidays = Holiday::all();

        if ($holidays){
            return JsonResponse::success('', $holidays);
        }

        return JsonResponse::error('No holiday found');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), Holiday::$rules);

        if ($validator->fails()) {
            return JsonResponse::error('Holiday not created', $validator->messages()->all(), 422);
        }

        $inputData = $request->all();

        $holiday = auth()->user()->holidays()->create($inputData);

        if ($holiday){
            return JsonResponse::success('Holiday created successfully', $holiday);
        }

        return JsonResponse::error('', ['not found']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function absentStore(Request $request)
    {
        $validator = Validator::make($request->all(), Holiday::$rules);

        if ($validator->fails()) {
            return JsonResponse::error('Holiday not created', $validator->messages()->all(), 422);
        }

        $user = User::find($request->user_id);

        $inputData = $request->all();

        $holiday = $user->holidays()->create($inputData);

        if ($holiday){
            return JsonResponse::success('Holiday created successfully', $holiday);
        }

        return JsonResponse::error('', ['not found']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Holiday  $holiday
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Holiday $holiday)
    {
        $validator = Validator::make($request->all(), Holiday::$rules);

        if ($validator->fails()) {
            return JsonResponse::error('Holiday not updated', $validator->messages()->all(), 422);
        }

        $inputData = $request->all();

        $check = $holiday->update($inputData);

        if ($check){
            return JsonResponse::success('Holiday updated successfully', $holiday);
        }

        return JsonResponse::error('', $holiday);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Holiday $holiday
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Holiday $holiday)
    {
        $check = $holiday->delete();

        if ($check){
            return JsonResponse::success('Holiday deleted successfully', $holiday);
        }

        return JsonResponse::error('', $holiday);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function users(Request $request)
    {
        $from = $request->from;
        $to = $request->to;

        $users = User::role('staff')->with(['holidays' => function($q) use ($from, $to) {
            $q->whereNull('end')->whereBetween('start', [$from, $to])
                ->orWhereNotNull('end')->whereBetween('start', [$from, $to])->whereBetween('end', [$from, $to])
                ->orWhere('start', '<', $from)->whereBetween('end', [$from, $to])
                ->orWhereBetween('start', [$from, $to])->where('end', '>', $to);
        }])->get();

        if ($users){
            return JsonResponse::success('', $users);
        }

        return JsonResponse::error('No user found');
    }
}
