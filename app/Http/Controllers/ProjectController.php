<?php

namespace App\Http\Controllers;

use App\Facades\JsonResponse;
use App\Models\Project;
use App\Rules\Url;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::with('staffs.user')->get();
        if ($projects){
            return JsonResponse::success('', $projects);
        }

        return JsonResponse::error('No record found');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customRules = [
            'url' => ['required','string','max:255','unique:projects', new Url],
            ];
        $validator = Validator::make($request->all(), Project::$rules + $customRules);

        if ($validator->fails()) {
            return JsonResponse::error('Project not created', $validator->messages()->all(), 422);
        }

        $inputData = $request->all();
        $project = Project::create($inputData);

        $project->staffs()->sync(collect($request->staffs)->pluck('value'));

        if ($project){
            return JsonResponse::success('Project created successfully', $project->load('staffs.user'));
        }

        return JsonResponse::error('Project not created', ['Project not created'], 422);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        $customRules = [
            'url' => ['required','string','max:255','unique:projects,url,'.$project->id, new Url],
        ];
        $validator = Validator::make($request->all(), Project::$rules + $customRules);

        if ($validator->fails()) {
            return JsonResponse::error('Project not updated', $validator->messages()->all(), 422);
        }

        $inputData = $request->all();
        $check = $project->update($inputData);

        $project->staffs()->sync(collect($request->staffs)->pluck('value'));

        if ($check){
            return JsonResponse::success('Project updated successfully', $project->load('staffs.user'));
        }

        return JsonResponse::error('Project not updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project $project
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Project $project)
    {
        $check = $project->delete();

        if ($check){
            return JsonResponse::success('Project deleted successfully', $project);
        }

        return JsonResponse::error('Project not deleted');
    }
}
