<?php

namespace App\Http\Controllers;

use App\Facades\JsonResponse;
use App\Models\Staff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staffs = Staff::with('user')->get();
//        $staffs = Staff::all();

        if ($staffs){
            return JsonResponse::success('Staff Data', $staffs);
        }

        return JsonResponse::error('No staff found', $staffs);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Staff $staff)
    {
        $validator = Validator::make($request->all(), Staff::$rules);

        if ($validator->fails()) {
            return JsonResponse::error('Staff not updated', $validator->messages()->all(), 422);
        }

        $check = $staff->update($request->all());

        if ($check){
            return JsonResponse::success('Record updated', $staff->user);
        }

        return JsonResponse::error('Record not updated', $staff->user);
    }
}
