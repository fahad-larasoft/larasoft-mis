<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Experience;
use App\Facades\JsonResponse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ExperienceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $experiences = auth()->user()->experiences;

        if ($experiences){
            return JsonResponse::success('', $experiences);
        }

        return JsonResponse::error('No experiences found');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), Experience::$rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()->all()], 422);
        }

        $inputData = $request->all();
        $inputData['company_url'] = 'https://www.apple.com/';
        $inputData['company_logo'] = 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Apple_logo_black.svg/300px-Apple_logo_black.svg.png';

        $experience =  auth()->user()->experiences()->create($inputData);

        if ($experience){
            return JsonResponse::success($experience->job_title . ' Experience created successfully', $experience);
        }

        return JsonResponse::error('No experiences found');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Experience  $experience
     * @return \Illuminate\Http\Response
     */
    public function show(Experience $experience)
    {
        if ($experience){
            return JsonResponse::success('', $experience);
        }

        return JsonResponse::error('No experiences found');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Experience  $experience
     * @return \Illuminate\Http\Response
     */
    public function edit(Experience $experience)
    {
        if ($experience){
            return JsonResponse::success('', $experience);
        }

        return JsonResponse::error('No experiences found');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Experience  $experience
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Experience $experience)
    {
        $validator = Validator::make($request->all(), Experience::$rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()->all()], 422);
        }

        $inputData = $request->all();
        $check = $experience->update($inputData);

        if ($check){
            return JsonResponse::success($experience->job_title . 'Updated successfully', $experience);
        }

        return JsonResponse::error('No experiences found');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Experience $experience
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Experience $experience)
    {
        $check = $experience->delete();

        if ($check){
            return JsonResponse::success('', $experience);
        }

        return JsonResponse::error('No experiences found');
    }
}
