<?php

namespace App\Http\Controllers\User;

use App\Facades\JsonResponse;
use App\Http\Controllers\Controller;
use App\Models\Holiday;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HolidayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $holidays = Holiday::whereIn('user_id', [1, auth()->id()])->get();

        if ($holidays){
            return JsonResponse::success('', $holidays);
        }

        return JsonResponse::error('No holiday found');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), Holiday::$rules);

        if ($validator->fails()) {
            return JsonResponse::error('Project not created', $validator->messages()->all(), 422);
        }

        $inputData = $request->all();
        $user = auth()->user();

        if($user->hasRole('admin')){
            $inputData['status'] = 'approved';
        }else{
            $inputData['status'] = 'pending';
        }

        $holiday = $user->holidays()->create($inputData);

        if ($holiday){
            return JsonResponse::success('Holiday created successfully', $holiday);
        }

        return JsonResponse::error('', $holiday);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Holiday  $holiday
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Holiday $holiday)
    {
        $validator = Validator::make($request->all(), Holiday::$rules);

        if ($validator->fails()) {
            return JsonResponse::error('Holiday not updated', $validator->messages()->all(), 422);
        }

        $inputData = $request->all();

        $check = $holiday->update($inputData);

        if ($check){
            return JsonResponse::success('Holiday updated successfully', $holiday);
        }

        return JsonResponse::error('', $holiday);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Holiday $holiday
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Holiday $holiday)
    {
        $check = $holiday->delete();

        if ($check){
            return JsonResponse::success('Holiday deleted successfully', $holiday);
        }

        return JsonResponse::error('', $holiday);
    }
}
