<?php

namespace App\Http\Controllers\User;

use App\Facades\JsonResponse;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Junaidnasir\Larainvite\Facades\Invite;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $users = User::all();
        return JsonResponse::success('', $users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), User::$rules, User::$messages);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()->all()], 422);
        }
        $data = $request->all();

        $data['password'] = bcrypt($request->password);

        User::create($data);

        return JsonResponse::success('User successfully added');
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        if ($user){
            return JsonResponse::success('', $user);
        }
        return JsonResponse::error('User not found');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return JsonResponse::success('', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $customRules = [
            'email' => 'required|unique:users,email,'.$user->id,
        ];

        $validator = Validator::make($request->all(), User::$rules + $customRules, User::$messages);

        if ($validator->fails()) {
            return JsonResponse::error('User not updated', $validator->messages()->all(), 422);
        }

        $data = $request->all();
        if ($request->image){
            $user->uploadImage($request->image);
        }

        if (isset($request->password)) {
            $data['password'] = bcrypt($request->password);
        }
        $check = $user->update($data);


        if ($check){
            return JsonResponse::success($user->name. ' User updated successfully', $user);
        }

        return JsonResponse::error('User not updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return JsonResponse::success('User Deleted Successfully.');
    }


    public function updatePassword(Request $request)
    {
        $this->validate($request, ['password' => 'required|min:6|confirmed']);

        auth()->user()->update(['password' => bcrypt($request->password)]);
        return response()->json(['success' => 1]);


    }

    public function invite(Request $request)
    {
        $refCode = Invite::invite($request->email , auth()->id());

        return JsonResponse::success('User Deleted Successfully.');
    }
}
