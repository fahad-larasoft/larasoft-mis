<?php

namespace App\Http\Controllers\User;

use App\Facades\JsonResponse;
use App\Http\Controllers\Controller;
use App\Models\Qualification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class QualificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $qualifications = auth()->user()->qualifications;

        if ($qualifications){
            return JsonResponse::success('', $qualifications);
        }

        return JsonResponse::error('No qualifications found');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), Qualification::$rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()->all()], 422);
        }

        $inputData = $request->all();

        $qualification = auth()->user()->qualifications()->create($inputData);

        if ($qualification){
            return JsonResponse::success($qualification->degree_title. ' Qualification created successfully', $qualification);
        }

        return JsonResponse::error('Data not saved');
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Qualification  $qualification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Qualification $qualification)
    {
        $validator = Validator::make($request->all(), Qualification::$rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()->all()], 422);
        }

        $inputData = $request->all();
        $check = $qualification->update($inputData);

        if ($check){
            return JsonResponse::success($qualification->degree_title. ' Qualification updated successfully', $qualification);
        }

        return JsonResponse::error('Data not updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Qualification $qualification
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Qualification $qualification)
    {
        $check = $qualification->delete();

        if ($check){
            return JsonResponse::success('', $qualification);
        }

        return JsonResponse::error('Data not deleted');
    }
}
