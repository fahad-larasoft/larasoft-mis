<?php

namespace App\Http\Controllers\User;

use App\Facades\JsonResponse;
use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\User;
use App\Rules\Url;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user =  User::find($request->user_id);
        $projects = [];
        if ($user->staff){
            $projects = $user->staff->projects;
        }

        if ($projects){
            return JsonResponse::success('', $projects);
        }

        return JsonResponse::error('No record found');
    }
}
