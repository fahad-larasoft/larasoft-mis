<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\File;
use Junaidnasir\Larainvite\InviteTrait;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRoles, InviteTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','first_name', 'last_name', 'date_of_birth', 'join_date',
    ];

    protected $with = ['roles', 'staff'];
    protected $appends = ['full_name'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'password' => 'nullable|confirmed|min:6'
    ];

    public static $messages = [
        'name.required' => 'Please enter name.',
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function addresses()
    {
        return $this->morphMany(Address::class, 'addressable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function experiences()
    {
        return $this->hasMany(Experience::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function holidays()
    {
        return $this->hasMany(Holiday::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function qualifications()
    {
        return $this->hasMany(Qualification::class);
    }

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->first_name . ' '.$this->last_name;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function staff()
    {
        return $this->hasOne(Staff::class);
    }

    /**
     * @param $image
     * @return void
     */
    public function uploadImage($image)
    {
        $exploded = explode(',', $image);
        $decoded = base64_decode($exploded[1]);

        if (str_contains($exploded[0], 'jpeg')) {
            $extension = 'jpg';
        } else {
            $extension = 'png';
        }

        $filename = str_random() . '.' . $extension;
        $path = public_path('/uploads/user/images/' . $filename);
        file_put_contents($path, $decoded);
        $imagePath = '/uploads/user/images/' . $filename;

        if (File::exists(public_path($this->image_url))){
            unlink(public_path($this->image_url));
        }

        $this->image_url = $imagePath;
    }

    public function appraisals()
    {
        return $this->hasMany(RatingUser::class);
    }
}
