<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'url',
        'assigned_date',
        'status',
    ];

    /**
     * @var array
     */
    public static $rules = [
        'name' => 'required|string|max:255',
//        'assigned_date' => 'required|date',
        'status' => 'required|string|max:255',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function staffs()
    {
        return $this->belongsToMany(Staff::class, 'project_staff', 'project_id', 'staff_id')
            ->withTimestamps();
    }

//    public function getStaffsIdsAttribute()
//    {
//        return $this->staffs()->pluck('id');
//    }
}

