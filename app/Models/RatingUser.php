<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RatingUser extends Model
{
    protected $table = 'ratings';

    protected $fillable = [
        'user_id',
        'rating_type_id',
        'points',
        'user_comment',
        'admin_comment',
    ];

    public static $rules = [
        'user_id' => 'required',
        'rating_type_id' => 'required',
        'points' => 'required',
        'user_comment' => 'nullable|string|max:255',
        'admin_comment' => 'nullable|string|max:255',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ratingType()
    {
        return $this->belongsTo(RatingType::class, 'rating_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
