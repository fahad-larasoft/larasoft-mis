<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $fillable = [
        'basic_pay',
        'joining_date',
        'leaving_date',
        'current_salary',
    ];

    public static $rules = [
        'joining_date' => 'required|date',
        'leaving_date' => 'nullable|date|after:joining_date',
        'basic_pay' => 'required|numeric|between:0,99999.99',
    ];
    

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function salaries()
    {
        return $this->hasMany(Salary::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function projects()
    {
        return $this->belongsToMany(Project::class, 'project_staff', 'staff_id', 'project_id')
            ->withTimestamps();
    }
}
