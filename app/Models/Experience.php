<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    protected $fillable = [
        'user_id',
        'job_title',
        'description',
        'start_date',
        'end_date',
        'company_name',
        'company_phone_number',
        'company_url',
        'company_logo',
    ];

    public static $rules = [
        'job_title' => 'required|string|max:255',
        'start_date' => 'required|date|before:today',
        'description' => 'nullable|string',
        'end_date' => 'nullable|date|after:start_date',
        'company_name' => 'required|string|max:255',
        'company_phone_number' => 'nullable|string|max:255',
        'company_url' => 'nullable|string|max:255',
        'company_logo' => 'nullable|string|max:255',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function address()
    {
        return $this->morphMany(Address::class, 'addressable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
