<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{
    protected $fillable = [
        'basic_salary',
        'bonus_amount',
        'deduction_amount',
        'overtime_amount',
        'status',
        'paying_date'
    ];

    public static $rules = [
        'basic_salary' => 'required|numeric|between:0,99999.99',
        'bonus_amount' => 'nullable|numeric|between:0,99999.99',
        'deduction_amount' => 'nullable|numeric|between:0,99999.99',
        'overtime_amount' => 'nullable|numeric|between:0,99999.99',
        'status' => 'required|string',
        'paying_date' => 'nullable|date'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function staff()
    {
        return $this->belongsTo(Staff::class);
    }
}
