<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Qualification extends Model
{
    protected $fillable = [
        'degree_title',
        'institute',
        'major',
        'start_date',
        'passing_year',
    ];

    public static $rules = [
        'degree_title' => 'required|string|max:255',
        'institute' => 'required|string|max:255',
        'major' => 'nullable|string|max:255',
        'start_date' => 'required|date|before:today',
        'passing_year' => 'nullable|date|after:start_date',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
