<?php

namespace App\Models;

use App\Utils\Globals\AppGlobals;
use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{
    protected $fillable = [
        'title',
        'start',
        'end',
        'approve_date',
        'status'
    ];

    protected $appends = ['backgroundColor', 'by'];

    public static $rules = [
        'title' => 'required|string',
        'start' => 'required|date',
        'end' => 'nullable|date|after:start_date',
        'approve_date' => 'nullable|date',
        'status' => 'nullable|string|max:255'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getBackgroundColorAttribute()
    {
        return $this->status ? AppGlobals::CALENDER_COLOR[$this->status] : null;
    }

    public function getByAttribute()
    {
        return $this->user->name;
    }
}
