<?php
/**
 * Created by PhpStorm.
 * User: larasoftio
 * Date: 4/5/18
 * Time: 5:08 PM
 */

namespace App\Helpers;


class helpers
{
    function isAdmin()
    {
        return auth()->user()->hasRole('admin');
    }
}