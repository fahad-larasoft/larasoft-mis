<?php
/**
 * Created by PhpStorm.
 * User: larasoftio
 * Date: 3/8/18
 * Time: 6:53 PM
 */

namespace App\Facades;


use Illuminate\Support\Facades\Facade;

class JsonResponse extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'JsonResponse';
    }
}