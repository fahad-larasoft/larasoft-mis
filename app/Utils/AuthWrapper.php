<?php
/**
 * Created by PhpStorm.
 * User: larasoftio
 * Date: 4/5/18
 * Time: 5:19 PM
 */

namespace App\Utils;


use App\Models\User;

class AuthWrapper
{
    public static function isAdmin()
    {
        return auth()->user()->hasRole('admin');
    }

    public static function admin()
    {
        return User::role('admin')->first();
    }

    public static function isStaff()
    {
        return auth()->user()->hasRole('staff');
    }
}