<?php
/**
 * Created by PhpStorm.
 * User: larasoftio
 * Date: 3/8/18
 * Time: 6:48 PM
 */

namespace App\Utils;


class JsonResult
{
    /**
     * @param null $message
     * @param null $data
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function success($message=null, $data=null)
    {
        return response([
            'success' => true,
            'error' => false,
            'message' => $message,
            'data' => $data
        ]);
    }

    /**
     * @param null $message
     * @param null $errors
     * @param int $response_code
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function error($message=null, $errors=null, $response_code = 422)
    {
        return response([
            'success' => false,
            'error' => true,
            'message' => $message,
            'errors' => $errors,
        ], $response_code);
    }
}