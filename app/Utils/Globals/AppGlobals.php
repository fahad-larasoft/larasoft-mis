<?php
/**
 * Created by PhpStorm.
 * User: larasoftio
 * Date: 4/2/18
 * Time: 6:23 PM
 */

namespace App\Utils\Globals;


class AppGlobals
{
    const CALENDER_COLOR = [
        'pending' => '#dfa726',
        'approved' => '#28873c',
        'rejected' => '#c62638',
        'cancel' => '#5a6267',
        'absent' => '#5a6267',
    ];
}