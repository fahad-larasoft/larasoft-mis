<?php

namespace App\Providers;

use App\Mail\userInvite;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Event::listen('junaidnasir.larainvite.invited', function ($invitation) {
            \Mail::to($invitation->email)->send(new userInvite($invitation));

//            \Mail::send('invitations.emailBody', $invitation->toArray(), function ($m) use ($invitation) {
//                $m->from('hello@larasoft.io', 'Larasoft.io');
//                $m->to($invitation->email);
//                $m->subject("You have been invited by ". $invitation->user->name);
//            });
        });
    }
}
