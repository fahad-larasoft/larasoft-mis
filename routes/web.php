<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});


Auth::routes();

Route::group(['prefix' => 'admin'], function () {
    Route::get('/', 'HomeController@index')->name('admin.home');

    /*********** User Routes ************/
    Route::resource('users', 'UsersController');

    /*********** Staff Routes ************/
    Route::resource('staffs', 'StaffController');


    /*********** Salaries Routes ************/
    Route::resource('salaries', 'SalaryController');

    /*********** Holidays Routes ************/
    Route::post('absent', 'HolidayController@absentStore');
    Route::get('holidays/users', 'HolidayController@users');
    Route::resource('holidays', 'HolidayController');

    /*********** Project Routes ************/
    Route::resource('projects', 'ProjectController');

    /*********** Rating types Routes ************/
    Route::resource('rating-types', 'RatingTypeController');

    /*********** Appraisals types Routes ************/
    Route::resource('appraisals', 'AppraisalController');

    /*********** Attendance types Routes ************/
    Route::resource('attendances', 'AttendanceController');
});

Route::group(['prefix' => 'user', 'namespace' => 'User'], function () {
    Route::get('/', 'HomeController@index')->name('user.home');

    /*********** Experiences Routes ************/
    Route::resource('experiences', 'ExperienceController');

    /*********** Qualifications Routes ************/
    Route::resource('qualifications', 'QualificationController');

    /*********** Users Routes ************/
    Route::resource('users', 'UsersController');

    /*********** Holidays Routes ************/
    Route::resource('holidays', 'HolidayController');

    /*********** Project Routes ************/
    Route::resource('projects', 'ProjectController');

    /*********** Salaries Routes ************/
    Route::resource('salaries', 'SalaryController');
});


Route::post('invite', 'UsersController@invite');


Route::post('/update-password', [
    'as' => 'users.password.update',
    'uses' => 'UsersController@updatePassword'
]);